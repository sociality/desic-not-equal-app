import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { APIAuthService } from '@desic/api';
import { NbAuthService } from '@nebular/auth';
import { tap } from 'rxjs/operators';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private authService: NbAuthService,
    private authenticateService: APIAuthService
  ) {}

  canActivate() {
    return this.authService.isAuthenticated().pipe(
      tap((authenticated) => {
        if (
          !authenticated ||
          this.authenticateService.isAdmin ||
          this.authenticateService.isSuperAdmin
        ) {
          localStorage.removeItem('auth_app_token');
          this.router.navigate(['/']);
        }
      })
    );
  }
}
