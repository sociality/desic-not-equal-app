import { Component, OnInit } from '@angular/core';

import { environment } from '../../../environments/environment';

@Component({
  selector: 'client-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.scss'],
})
export class VersionComponent implements OnInit {
  public _name = `${environment.name}`;
  public _version = `${environment.version}`;
  public _text = `This demo version &quot;${this._version}&quot; may contain errors. It is provided for limited demostration only.`;
  constructor() {}

  ngOnInit(): void {}
}
