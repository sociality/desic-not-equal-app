import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeerInitializeComponent } from './peer-initialize.component';

describe('PeerInitializeComponent', () => {
  let component: PeerInitializeComponent;
  let fixture: ComponentFixture<PeerInitializeComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PeerInitializeComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PeerInitializeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
