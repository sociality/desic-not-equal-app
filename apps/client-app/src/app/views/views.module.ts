import { PdfDownloadComponent } from './pdf-download/pdf-download.component';
import { PageLoadingComponent } from './page-loading/page-loading.component';
import { PageNoContentComponent } from './page-no_content/page-no_content.component';
import { PeerInitializeComponent } from './peer-initialize/peer-initialize.component';
import { AccessManagementComponent } from './access-management/access-management.component';
import { VersionComponent } from './version/version.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ColorPickerModule } from 'ngx-color-picker';
import {
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbTabsetModule,
  NbSelectModule,
  NbButtonModule,
  NbDialogModule,
  NbListModule,
  NbIconModule,
  NbTooltipModule,
  NbSpinnerModule,
  NbTreeGridModule,
  NbBadgeModule,
  NbUserModule,
  NbActionsModule,
} from '@nebular/theme';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { RouterModule } from '@angular/router';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [
    AccessManagementComponent,
    PdfDownloadComponent,
    PageLoadingComponent,
    PageNoContentComponent,
    PeerInitializeComponent,
    VersionComponent,
  ],
  imports: [
    CommonModule,

    FormsModule,
    ReactiveFormsModule,

    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbButtonModule,
    NbTabsetModule,
    NbTreeGridModule,
    NbSelectModule,
    NbCardModule,
    NbDialogModule.forRoot(),
    NbListModule,
    NbIconModule,
    NbEvaIconsModule,
    NbTooltipModule,
    NbSpinnerModule,
    NbBadgeModule,
    NbUserModule,
    NbActionsModule,

    ColorPickerModule,

    RouterModule,
    CKEditorModule,

    PipesModule,
  ],
  exports: [
    AccessManagementComponent,
    PdfDownloadComponent,
    PageLoadingComponent,
    PageNoContentComponent,
    PeerInitializeComponent,
    VersionComponent,
  ],
})
export class ViewsModule {}
