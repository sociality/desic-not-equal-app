import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'client-page-no_content',
  templateUrl: './page-no_content.component.html',
  styleUrls: ['./page-no_content.component.scss'],
})
export class PageNoContentComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
