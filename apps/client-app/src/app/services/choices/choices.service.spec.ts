import { TestBed } from '@angular/core/testing';

import { AppChoicesService } from './choices.service';

describe('StateService', () => {
  let service: AppChoicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppChoicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
