import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { InternalChoiceList, InternalContactList } from '@desic/models';

@Injectable({
  providedIn: 'root',
})
export class AppChoicesService {
  constructor() {}

  /**
   * Contacts List
   */
  contactsList: InternalContactList[] = [
    // {
    //     slug: 'Phone',
    //     title: 'FIELDS.PROFILE.CONTACT_CHOICES.A',
    //     name: 'Phone',
    //     icon: 'phone',
    //     value: '',
    //     description: '',
    // }
    {
      slug: 'WEB',
      prefix: 'https://www.',
      title: 'FIELDS.PROFILE.CONTACT_CHOICES.B',
      name: 'Website',
      icon: 'browser-outline',
      value: '',
      description: '',
    },
    {
      slug: 'FB',
      prefix: 'https://www.facebook.com/',
      title: 'FIELDS.PROFILE.CONTACT_CHOICES.C',
      name: 'Facebook',
      icon: 'facebook-outline',
      value: '',
      description: '',
    },
    {
      slug: 'TW',
      prefix: 'https://twitter.com/',
      title: 'FIELDS.PROFILE.CONTACT_CHOICES.D',
      name: 'Twitter',
      icon: 'twitter-outline',
      value: '',
      description: '',
    },
    // {
    //   slug: 'IG',
    //   prefix: 'https://www.instagram.com/',
    //   title: 'FIELDS.PROFILE.CONTACT_CHOICES.E',
    //   name: 'Instagram',
    //   icon: 'instagram',
    //   value: '',
    //   description: '',
    // },
    // {
    //   slug: 'YT',
    //   prefix: 'https://www.youtube.com/channel/',
    //   title: 'FIELDS.PROFILE.CONTACT_CHOICES.F',
    //   name: 'Youtube',
    //   icon: 'youtube',
    //   value: '',
    //   description: '',
    // },
  ];

  public get getContactsList(): InternalContactList[] {
    return this.contactsList;
  }

  /**
   * Aministrator Category List
   */
  administratorCategoryList: InternalChoiceList[] = [
    {
      title: 'Law',
      name: 'law',
      value: '40',
    },
    {
      title: 'Medial',
      name: 'medical',
      value: '30',
    },
    {
      title: 'Social',
      name: 'social',
      value: '20',
    },
    {
      title: 'Manager',
      name: 'manager',
      value: '10',
    },
  ];

  public get getAdministratorCategoryList(): InternalChoiceList[] {
    return this.administratorCategoryList;
  }

  /**
   * Marital Status List
   */
  maritalStatusList: InternalChoiceList[] = [
    {
      title: 'Single',
      name: 'single',
      value: 'single',
    },
    {
      title: 'In a relationship',
      name: 'relationship',
      value: 'relationship',
    },
    {
      title: 'Engaged',
      name: 'engaged',
      value: 'engaged',
    },
    {
      title: 'Married',
      name: 'married',
      value: 'married',
    },
    {
      title: 'Widowed',
      name: 'widowed',
      value: 'widowed',
    },
    {
      title: 'Divorced',
      name: 'divorced',
      value: 'divorced',
    },
  ];

  public get getMaritalStatusList(): InternalChoiceList[] {
    return this.maritalStatusList;
  }

  /**
   * Employment Status List
   */
  employmentStatusList: InternalChoiceList[] = [
    {
      title: 'Employee',
      name: 'employee',
      value: 'employee',
    },
    {
      title: 'Unemployed',
      name: 'unemployed',
      value: 'unemployed',
    },
    {
      title: 'Retired',
      name: 'retired',
      value: 'retired',
    },
    {
      title: 'Other',
      name: 'other',
      value: 'other',
    },
  ];

  public get getEmploymentStatusList(): InternalChoiceList[] {
    return this.employmentStatusList;
  }

  accessStatusList: InternalChoiceList[] = [
    {
      name: 'Access',
      slug: 'access',
      value: 'invoke',
    },
    {
      name: 'Access is recommended',
      slug: 'neutral',
      value: 'neutral',
    },
    {
      name: 'No Access',
      slug: 'no_access',
      value: 'revoke',
    },
  ];

  public get getAccessStatusList(): InternalChoiceList[] {
    return this.accessStatusList;
  }
}
