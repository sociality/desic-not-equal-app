import { AppStateService } from './services/state/state.service';

import { ThemeModule } from './@theme/theme.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AuthGuard } from './auth-guard.service';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NbThemeModule,
  NbLayoutModule,
  NbMenuModule,
  NbIconModule,
  NbCardBackComponent,
  NbCardModule,
  NbInputModule,
  NbButtonModule,
} from '@nebular/theme';
import {
  NbPasswordAuthStrategy,
  NbAuthModule,
  NbAuthJWTToken,
  NbAuthJWTInterceptor,
  NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
} from '@nebular/auth';

import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ClientAppRoutingModule } from './app-routing.module';
//import { HttpClientModule } from '@angular/common/http';
import { ClientHomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { AppPostsModule } from './pages/posts/posts.module';
import { AppDocumentsModule } from './pages/documents/documents.module';
// import { ClientAuthModule } from './pages/auth/auth.module';

import { ApiModule, AppConfig } from '@desic/api';

//import { TranslateModule } from '@ngx-translate/core';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';

import {
  MissingTranslationHandler,
  MissingTranslationHandlerParams,
} from '@ngx-translate/core';

import { PipesModule } from './pipes/pipes.module';
import { ViewsModule } from './views/views.module';
import { CommonModule } from '@angular/common';
import { AccessComponent } from './pages/access/access.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18/', '.json');
}

export class MyMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    return '<<this text is untranslated>>';
  }
}

const ANGULAR_MODULES = [
  BrowserModule,
  BrowserAnimationsModule,
  HttpClientModule,
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
];

const NEBULAR_MODULES = [
  NbThemeModule.forRoot({ name: 'default' }),
  NbLayoutModule,
  NbEvaIconsModule,
  NbMenuModule.forRoot(),
  NbIconModule,
  NbCardModule,
  NbInputModule,
  NbButtonModule,

  NbAuthModule.forRoot({
    strategies: [
      NbPasswordAuthStrategy.setup({
        name: 'email',
        token: {
          class: NbAuthJWTToken,
          key: 'token',
        },
        baseEndpoint: AppConfig.API_URL,
        login: {
          endpoint: '/authentication/sign_in',
          method: 'post',
          redirect: {
            success: '/', // '/' + {outlets: null},//'/',
            failure: null, // stay on the same page
          },
        },
        logout: {
          endpoint: '/authentication/sign_out',
          method: 'delete',
          redirect: {
            success: '/', // '/auth/login',
            failure: null, // stay on the same page
          },
        },
        requestPass: {
          endpoint: '/authentication/request_pass',
          method: 'post',
          redirect: {
            success: '/auth/login',
            failure: null, // stay on the same page
          },
        },
        resetPass: {
          endpoint: '/authentication/restore_pass',
          method: 'post',
          resetPasswordTokenKey: 'tokenKey',
          redirect: {
            success: '/auth/login',
            failure: null, // stay on the same page
          },
        },
      }),
    ],
    forms: {},
  }),
];

const CLIENT_MODULES = [
  //AppPostsModule, AppDocumentsModule,
  // ClientAuthModule,
  ClientAppRoutingModule,
  ThemeModule,
];

const TRANSLATE_MODULE = [
  TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: HttpLoaderFactory,
      deps: [HttpClient],
    },
    missingTranslationHandler: {
      provide: MissingTranslationHandler,
      useClass: MyMissingTranslationHandler,
    },
    useDefaultLang: true,
  }),
];

@NgModule({
  declarations: [
    AppComponent,
    ClientHomeComponent,
    AccessComponent,
    ContactComponent,
  ],
  imports: [
    ...ANGULAR_MODULES,
    ...NEBULAR_MODULES,
    ...TRANSLATE_MODULE,

    ...CLIENT_MODULES,

    PipesModule,
    ViewsModule,
  ],
  exports: [TranslateModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: NbAuthJWTInterceptor, multi: true },
    {
      provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
      useValue: function () {
        return false;
      },
    },
    AuthGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
