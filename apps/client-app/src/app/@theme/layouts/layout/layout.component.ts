import { Observable } from 'rxjs';
import { AppStateService } from '../../../services/state/state.service';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'client-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class ClientLayoutComponent {
  title = 'client-app';
  pageTitle$: Observable<string>;

  constructor(
    public translate: TranslateService,
    private readonly appStateService: AppStateService
  ) {
    this.pageTitle$ = this.appStateService.pageTitle$;
    translate.addLangs(['en']);
    translate.setDefaultLang('en');
  }
}
