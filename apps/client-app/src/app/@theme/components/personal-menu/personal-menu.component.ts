import { Router } from '@angular/router';
import {
  ChangeDetectorRef,
  Component,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { NbDialogRef, NbDialogService, NbMenuItem } from '@nebular/theme';

import { AuthRequestPasswordComponent } from '../../../pages/auth/auth-request-password/auth-request-password.component';
import { AuthLoginComponent } from '../../../pages/auth/auth-login/auth-login.component';
import { AuthUserInterface } from '@desic/models';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { Observable, Subscription } from 'rxjs';
import { LocalConnectionService } from '../../../pages/connections/_connection.service';
import { LocalConnectionInterface } from '../../../pages/connections/_connection.interface';
import { PERSONAL_MENU_ITEMS } from '../../../menu-items.config';

interface MenuItem extends NbMenuItem {
  slug: string;
}

@Component({
  selector: 'client-personal-menu',
  templateUrl: './personal-menu.component.html',
  styleUrls: ['./personal-menu.component.scss'],
})
export class ClientPersonalMenuComponent implements OnInit {
  items: MenuItem[] = PERSONAL_MENU_ITEMS;

  token: NbAuthJWTToken;

  public sessions: LocalConnectionInterface['PeerSessions'] = [];
  private subscription: Subscription = new Subscription();

  constructor(
    private router: Router,
    private readonly translate: TranslateService,
    private changeDetectorRef: ChangeDetectorRef,
    private dialogService: NbDialogService,
    private authService: NbAuthService,
    private localConnectionService: LocalConnectionService
  ) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      this.token = token;
    });

    this.subscription.add(
      this.localConnectionService.peerSessions.subscribe(
        (data) => (this.sessions = data),
        (error) => {
          throw error;
        }
      )
    );

    this.localConnectionService.peerSessions.subscribe(
      (data) => {
        setTimeout(() => {
          this.items[
            this.items.findIndex((obj) => obj.title === 'Connections')
          ]['badge'] = {
            text: this.getTotalUnread().toString(),
            status: 'warning',
          };
        });
        this.changeDetectorRef.markForCheck();
      },
      (error) => {
        throw error;
      }
    );
  }

  ngOnInit(): void {}

  getTotalUnread() {
    return this.sessions.reduce((accum, item) => accum + item.unread, 0);
  }
}
