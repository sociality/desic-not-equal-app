import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { APIAuthService, APILanguagesService } from '@desic/api';
import { LanguageInterface } from '@desic/models';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'client-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class ClientHeaderComponent implements OnInit {
  @Input() title: string;

  // menuItems = [
  //   { title: 'About DESIC', link: '/' },
  //   { title: 'Events', link: '/posts/list' },
  //   {
  //     title: 'Contact',
  //     url: 'mailto:info@desicnetwork.org',
  //     target: '_blank',
  //   },
  // ];

  languages: LanguageInterface[];
  token: NbAuthJWTToken;

  constructor(
    private readonly router: Router,
    public translate: TranslateService,
    private authService: NbAuthService,
    private authenticateService: APIAuthService
  ) {
    this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
      this.token = token;
      if (!token.isValid() || !this.authenticateService.isClient) {
        localStorage.removeItem('auth_app_token');
      }
    });
  }

  logoLegend = 'DESIC application';
  ngOnInit(): void {}

  isLoggedIn() {
    // this.authService.onTokenChange().subscribe((token: NbAuthJWTToken) => {
    // this.token = token;
    return this.token.isValid(); // this.token$;
    // });
  }

  open() {
    this.router.navigateByUrl('auth/login');
    // const currentURL = this.router.url;
    // this.dialogService.open(AuthModalComponent, {

    // }).onClose.subscribe((value) => {
    //   console.log("onClose");
    //   console.log(currentURL)
    //   this.router.navigate([{ outlets: { modal: null } }])
    // });
  }

  goHome() {
    this.router.navigate(['/']);
  }
}
