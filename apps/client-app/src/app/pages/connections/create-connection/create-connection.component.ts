import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { Router } from '@angular/router';
import { APIConnectionsService } from '@desic/api';
import { AppStateService } from '../../../services/state/state.service';
import { ConnectionInterface } from '@desic/models';
import { Observable, Subject, Subscription } from 'rxjs';
import { LocalConnectionService } from '../_connection.service';
import { LocalConnectionInterface } from '../_connection.interface';

@Component({
  selector: 'client-create-connection',
  templateUrl: './create-connection.component.html',
  styleUrls: ['./create-connection.component.scss'],
})
export class CreateConnectionComponent implements OnInit, OnDestroy {
  @ViewChild('response_dialog') response_dialog;

  desicConnections$: Observable<ConnectionInterface[]>;
  desicConnections: LocalConnectionInterface['DesicConnections'];

  inviteForm: FormGroup;

  private unsubscribe: Subject<any>;
  private subscription: Subscription = new Subscription();

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private router: Router,
    private readonly connectionService: APIConnectionsService,
    private localConnectionService: LocalConnectionService,
    private readonly appStateService: AppStateService
  ) {
    this.subscription.add(
      this.localConnectionService.desicConnections.subscribe(
        (data) => (this.desicConnections = data)
      )
    );
  }

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.localConnectionService.changeDesicConnection(null);
    this.subscription.unsubscribe();
  }

  initForm() {
    this.inviteForm = this.fb.group({
      connectionId: [, Validators.compose([Validators.required])],
    });
  }

  private loadConnections() {
    this.desicConnections$ = this.connectionService.getConnectionsClient();
    this.localConnectionService.changeAnything(this.desicConnections$);
    this.desicConnections$.subscribe((data) => {
      this.desicConnections = data;
      this.localConnectionService.changeDesicConnections(this.desicConnections);
    });
  }

  public onFormSubmit() {
    const controls = this.inviteForm.controls;
    if (this.inviteForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.connectionService
      .postConnection(controls.connectionId.value)
      .subscribe(
        (data) => {
          console.log(data);
          this.loadConnections();
          this.openResponseDialog(this.response_dialog, {
            title: 'OK',
            message: 'Contact has been added',
          });
        },
        (error) => {
          this.openResponseDialog(this.response_dialog, {
            title: 'Error',
            message: error.message,
          });
        }
      );
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      // this.router.navigateByUrl('/members');
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.inviteForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
