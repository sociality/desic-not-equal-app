import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { BehaviorSubject } from 'rxjs';

import { LocalConnectionInterface } from './_connection.interface';

@Injectable()
export class LocalConnectionService {
  private peerSource = new BehaviorSubject('');
  peer = this.peerSource.asObservable();

  private peerSessionsSource = new BehaviorSubject([
    {
      peer: '',
      connectionId: '',
      secret: '',
      //    messages: [{ sender: '', text: '' }],
      messages: [
        {
          text: '',
          date: new Date(),
          type: 'text',
          files: [],
          reply: true,
          user: { name: '' },
        },
      ],
      unread: 0,
    },
  ]);
  peerSessions = this.peerSessionsSource.asObservable();

  private desicConnectionSource = new BehaviorSubject({
    id: '',
    secret: '',
    sender: {
      id: '',
      email: '',
      name: '',
      access: 0,
    },
    senderId: '',
    receiver: {
      id: '',
      email: '',
      name: '',
      access: 0,
    },
    receiverId: '',
  });
  desicConnection = this.desicConnectionSource.asObservable();

  private desicConnectionsSource = new BehaviorSubject([]);
  // private desicConnectionsSource = new BehaviorSubject([
  //   {
  //     id: '',
  //     secret: '',
  //     sender: {
  //       id: '',
  //       email: '',
  //       name: '',
  //       access: 0,
  //     },
  //     senderId: '',
  //     receiver: {
  //       id: '',
  //       email: '',
  //       name: '',
  //       access: 0,
  //     },
  //     receiverId: '',
  //   },
  // ]);
  desicConnections = this.desicConnectionsSource.asObservable();

  // private actionsSource = new BehaviorSubject({
  //     registration: 'xxxxxx'
  // });
  // actions = this.actionsSource.asObservable();

  private anythingSource = new BehaviorSubject(
    new Observable<LocalConnectionInterface['DesicConnections']>()
  );
  anything = this.anythingSource.asObservable();

  constructor() {}

  changeAnything(
    Observable: Observable<LocalConnectionInterface['DesicConnections']>
  ) {
    this.anythingSource.next(Observable);
  }

  changePeer(peer: any) {
    this.peerSource.next(peer);
  }

  changeDesicConnection(
    desicConnection: LocalConnectionInterface['DesicConnection']
  ) {
    this.desicConnectionSource.next(desicConnection);
  }

  changeDesicConnections(
    desicConnections: LocalConnectionInterface['DesicConnections']
  ) {
    this.desicConnectionsSource.next(desicConnections);
  }

  changePeerSessions(peerSessions: LocalConnectionInterface['PeerSessions']) {
    this.peerSessionsSource.next(peerSessions);
  }

  // changeActions(actions: LocalConnectionInterface["Actions"]) {
  //     this.actionsSource.next(actions);
  // }
}
