import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  Input,
  OnDestroy,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { APIAuthService, APIConnectionsService } from '@desic/api';
import { AppStateService } from '../../../services/state/state.service';
import Peer from 'peerjs';

import { AuthUserInterface, ConnectionInterface } from '@desic/models';
import { finalize, takeUntil, takeWhile } from 'rxjs/operators';

import * as CryptoJS from 'crypto-js';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';

/** Local Service & Interface */
import { LocalConnectionService } from '../_connection.service';
import { LocalConnectionInterface } from '../_connection.interface';
import { ClientConnectionsModule } from '../connections.module';

@Component({
  selector: 'client-peer-connection',
  templateUrl: './peer-connection.component.html',
  styleUrls: ['./peer-connection.component.scss'],
})
export class PeerConnectionComponent implements OnInit, OnDestroy, OnChanges {
  @Input() connectionId: number;

  isOpen: boolean = null;

  public mypeerid = '';
  public communicationId = '';
  public peer: any; //PeerInterface;
  public conn: any;

  public desicConnection: LocalConnectionInterface['DesicConnection'];
  public sessions: LocalConnectionInterface['PeerSessions'];

  private unsubscribe: Subject<any>;
  private subscription: Subscription = new Subscription();

  constructor(
    private readonly connectionService: APIConnectionsService,
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private readonly authService: APIAuthService,
    private readonly appStateService: AppStateService,
    private localConnectionService: LocalConnectionService
  ) {
    this.mypeerid = (this.authService.currentUser as AuthUserInterface)._id;
    this.communicationId = (this.authService
      .currentUser as AuthUserInterface).communicationId;
    this.subscription.add(
      this.localConnectionService.peer.subscribe((data) => (this.peer = data))
    );
    this.subscription.add(
      this.localConnectionService.peerSessions.subscribe(
        (data) => (this.sessions = data)
      )
    );
    this.subscription.add(
      this.localConnectionService.desicConnection.subscribe(
        (data) => (this.desicConnection = data)
      )
    );
  }

  ngOnInit(): void {
    // setTimeout(() => {
    //   this.appStateService.setPageTitle('Connection - MyID: ' + this.communicationId);
    // });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('Connection Change');
    this.setCurrentOutgoingConn();
    // this.getCurrentOutgoingConn();
  }

  setCurrentOutgoingConn() {
    const conn = this.peer.connect(this.desicConnection.receiver.id.toString());
    this.isOpen = conn.open;

    this.peer.on('error', (error: any) => {
      this.isOpen = false;
      this.changeDetectorRef.detectChanges();
    });
    this.conn = conn;
    return conn;
  }

  getCurrentOutgoingConn() {
    const conn = this.peer.connect(this.desicConnection.receiver.id.toString());

    this.peer.on('error', (error: any) => {
      this.isOpen = false;
      this.conn = conn;
      this.changeDetectorRef.detectChanges();
    });
    return conn;
  }

  getConnectionStatus(isOpen) {
    switch (isOpen) {
      case true:
        return 'success';
        // code block
        break;
      case false:
        return 'danger';
        // code block
        break;
      case null:
        return 'warning';
        // code block
        break;
      default:
        return 'warning';
    }
  }

  getConnectionTitle(isOpen) {
    switch (isOpen) {
      case true:
        return `Messages with ${this.desicConnection.receiver.name}`;
        break;
      case false:
        return `Messages with ${this.desicConnection.receiver.name} (offline)`;
        // code block
        break;
      default:
        return `Messages with ${this.desicConnection.receiver.name} (...)`;
    }
  }

  encryptData(data: string, secret: string) {
    try {
      return CryptoJS.AES.encrypt(JSON.stringify(data), secret).toString();
    } catch (error) {
      console.log(error);
    }
  }

  decryptData(data: any, secret: string) {
    try {
      const bytes = CryptoJS.AES.decrypt(data, secret);
      if (bytes.toString()) {
        return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      return data;
    } catch (error) {
      console.log(error);
    }
  }

  public onFormSubmit(event) {
    console.log(this.conn.open);
    if (!this.conn.open) return;

    const conn = this.getCurrentOutgoingConn();
    console.log('Connection', conn);

    conn.on('open', () => {
      const message = this.encryptData(
        event.message, // controls.text.value,
        this.desicConnection.secret
      );
      this.storeMessages(conn, message);
      this.changeDetectorRef.detectChanges();

      conn.send(message);
    });

    conn.on('error', (error: any) => {
      console.log('Error 1 Send: ' + error);
    });

    this.peer.on('error', (error: any) => {
      console.log('Error 1 Init: ' + error);
    });
  }

  returnMessages() {
    const index = this.sessions
      .map((o) => {
        return o.connectionId;
      })
      .indexOf(this.desicConnection.id);

    if (index < 0) {
      return [];
    } else {
      return this.sessions[index].messages;
    }
  }

  storeMessages(conn, message: string) {
    console.log('Store Messages on Send');

    const secret: string = this.desicConnection.secret;
    const connectionId: string = this.desicConnection.id;

    if (this.sessions && !this.sessions[0].connectionId) {
      this.sessions[0] = {
        peer: conn.peer,
        secret: secret,
        connectionId: connectionId,
        messages: [
          this.messageFormat(this.decryptData(message, secret)),
          // { sender: 'me', text: this.decryptData(message, secret) }
        ],
        unread: 0,
      };
      // if (!this.sessions || this.sessions.length < 1) {
      //   this.sessions = [{ peer: conn.peer, secret: secret, connectionId: connectionId, messages: [{ sender: 'me', text: this.decryptData(message, secret) }], unread: [] }];
    } else {
      const index = this.sessions
        .map((o) => {
          return o.connectionId;
        })
        .indexOf(connectionId);

      if (index < 0) {
        this.sessions.push({
          peer: conn.peer,
          secret: secret,
          connectionId: connectionId,
          //messages: [{ sender: 'me', text: this.decryptData(message, secret) }],
          messages: [this.messageFormat(this.decryptData(message, secret))],

          unread: 0,
        });
      } else {
        this.sessions[index].messages.push(
          //   {
          //   sender: 'me',
          //   text: this.decryptData(message, secret),
          // }
          this.messageFormat(this.decryptData(message, secret))
        );
      }
      this.localConnectionService.changePeerSessions(this.sessions);

      console.log('My messages');
      console.log(this.sessions);
    }
  }

  public messageFormat(message: string) {
    // const files = !event.files ? [] : event.files.map((file) => {
    //   return {
    //     url: file.src,
    //     type: file.type,
    //     icon: 'file-text-outline',
    //   };
    // });

    return {
      text: message,
      date: new Date(),
      reply: true,
      type: 'text', //files.length ? 'file' : 'text',
      files: [], //files,
      user: {
        name: this.desicConnection.sender.name,
        //avatar: 'https://i.gifer.com/no.gif',
      },
    };
  }
}
