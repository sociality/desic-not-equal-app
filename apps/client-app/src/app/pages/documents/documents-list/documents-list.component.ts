import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  APIExaminationsService,
  APIIncidentsService,
  APIOrganizationService,
  APISessionsService,
  APIUsersService,
} from '@desic/api';
import {
  ExaminationInterface,
  IncidentInterface,
  OrganizationInterface,
  SessionInterface,
} from '@desic/models';
import { NbDialogService, NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { AppStateService } from '../../../services/state/state.service';
import { combineLatest } from 'rxjs/operators/combineLatest';

@Component({
  selector: 'client-documents-list',
  templateUrl: './documents-list.component.html',
  styleUrls: ['./documents-list.component.scss'],
})
export class DocumentsListComponent implements OnInit {
  incidents$: Observable<IncidentInterface[]>;
  sessions$: Observable<SessionInterface[]>;
  examinations$: Observable<ExaminationInterface[]>;

  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private iconLibraries: NbIconLibraries,
    private readonly translate: TranslateService,
    private readonly incidentsService: APIIncidentsService,
    private readonly sessionsService: APISessionsService,
    private readonly examinationsService: APIExaminationsService,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('My Documents');
    });
    this.loadIncidents();
    this.loadSessions();
    this.loadExaminations();
  }

  get combined$() {
    return combineLatest(
      this.incidents$,
      this.sessions$,
      this.examinations$,
      (incidents, sessions, examinations) =>
        incidents || sessions || examinations
    );
  }

  private loadIncidents() {
    this.incidents$ = this.incidentsService.getIncidentsByClient();
  }

  private loadSessions() {
    this.sessions$ = this.sessionsService.getSessionsByClient();
  }

  private loadExaminations() {
    this.examinations$ = this.examinationsService.getExaminationsByClient();
  }

  public onClickPreview(
    type: string,
    item: IncidentInterface | SessionInterface | ExaminationInterface
  ) {
    console.log(`/documents/preview/${type}/${item.id}`);
    this.router.navigate([`/documents/preview/${type}/${item.id}`]);
  }

  public onClickExportToPDF(
    type: string,
    item: IncidentInterface | SessionInterface | ExaminationInterface
  ) {
    return;
  }
}
