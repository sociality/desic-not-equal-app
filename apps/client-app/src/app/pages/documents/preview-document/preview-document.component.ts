import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import {
  APIExaminationsService,
  APIIncidentsService,
  APISessionsService,
} from '@desic/api';
import {
  ExaminationInterface,
  IncidentInterface,
  SessionInterface,
} from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

import jspdf from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'preview-document',
  templateUrl: './preview-document.component.html',
  styleUrls: ['./preview-document.component.scss'],
})
export class PreviewDocumentComponent implements OnInit, OnDestroy {
  @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;
  @ViewChild('delete_dialog') delete_dialog;

  item$: Observable<
    IncidentInterface | SessionInterface | ExaminationInterface
  >;
  type: string;
  canExport: boolean = false;

  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly incidentsService: APIIncidentsService,
    private readonly sessionsService: APISessionsService,
    private readonly examinationsService: APIExaminationsService,
    private readonly appStateService: AppStateService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Incidents');
    });
    this.fetchParameter();
    this.canExport = true;
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.patching = false;
    this.sending = false;
  }

  private loadIncident(id: string) {
    return this.incidentsService.getIncidentById(id);
  }

  private loadSession(id: string) {
    return this.sessionsService.getSessionById(id);
  }

  private loadExamination(id: string) {
    return this.examinationsService.getExaminationById(id);
  }

  private itemType(type: string, id: string) {
    switch (type) {
      case 'incident':
        this.item$ = this.loadIncident(id);
        break;
      case 'session':
        this.item$ = this.loadSession(id);
        break;
      case 'examination':
        this.item$ = this.loadExamination(id);
        break;
      default:
      // code block
    }
  }
  s;

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        tap(
          (params) => {
            const id = params['id'];
            const type = (this.type = params['type']);
            console.log(id, type);
            if (id) {
              this.itemType(type, id);
              this.patching = true;
            } else {
              this.patching = true;
              return;
            }
          },
          (error) => {
            throw error;
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  public onClickBack() {
    this.router.navigateByUrl(`/documents/list`);
  }

  // public onClickEdit(incidentID: string) {
  //   this.router.navigateByUrl(`/incidents/edit/${incidentID}`);
  // }

  // public onClickDelete(incidentID: string, title: string) {
  //   this.openDeleteDialog(incidentID, title);
  // }

  // onClickExport(contentToConvert) {
  //   console.log(contentToConvert);
  //   console.log(typeof contentToConvert)
  //   let data = document.getElementById(contentToConvert);
  //   html2canvas(data).then((canvas) => {
  //     console.log(canvas)
  //     const contentDataURL = canvas.toDataURL('image/png');
  //     let pdf = new jspdf('l', 'cm', 'a4'); //Generates PDF in landscape mode
  //     // let pdf = new jspdf('p', 'cm', 'a4'); Generates PDF in portrait mode
  //     pdf.addImage(contentDataURL, 'PNG', 0, 0, 29.7, 21.0);
  //     pdf.save('Filename.pdf');
  //   });
  // }

  onClickExport(data) {
    html2canvas(data, { allowTaint: true }).then((canvas) => {
      let HTML_Width = canvas.width;
      let HTML_Height = canvas.height;
      let top_left_margin = 15;
      let PDF_Width = HTML_Width + top_left_margin * 2;
      let PDF_Height = PDF_Width * 1.5 + top_left_margin * 2;
      let canvas_image_width = HTML_Width;
      let canvas_image_height = HTML_Height;
      let totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
      canvas.getContext('2d');
      let imgData = canvas.toDataURL('image/jpeg', 1.0);
      let pdf = new jspdf('p', 'pt', [PDF_Width, PDF_Height]);
      pdf.addImage(
        imgData,
        'JPG',
        top_left_margin,
        top_left_margin,
        canvas_image_width,
        canvas_image_height
      );
      for (let i = 1; i <= totalPDFPages; i++) {
        pdf.addPage([PDF_Width, PDF_Height], 'p');
        pdf.addImage(
          imgData,
          'JPG',
          top_left_margin,
          -(PDF_Height * i) + top_left_margin * 4,
          canvas_image_width,
          canvas_image_height
        );
      }
      pdf.save('HTML-Document.pdf');
    });
  }

  deleteIncident(incidentID: string) {
    this.responseHandler(this.incidentsService.deleteIncident(incidentID));
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = true;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openDeleteDialog(incidentID: string, title: string) {
    const dialogRef = this.dialogService.open(this.delete_dialog, {
      context: { slug: title },
    });
    dialogRef.onClose.subscribe((result) => {
      if (result) {
        this.deleteIncident(incidentID);
      }
    });
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/incidents');
    });
  }
}

// private fetchParameter() {
//   this.activatedRoute.params.subscribe((params) => {
//     const id = params['id'];
//     if (id) {
//       this.loadIncident(id).subscribe((data) => {
//         this.incident = data;
//         this.patching = true;
//         this.changeDetectorRef.markForCheck();
//       });
//     } else {
//       this.patching = true;
//     }
//   }),
//     finalize(() => {
//       this.changeDetectorRef.markForCheck();
//     });
// }
