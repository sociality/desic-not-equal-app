import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentsListComponent } from './documents-list/documents-list.component';
import { PreviewDocumentComponent } from './preview-document/preview-document.component';

const routes: Routes = [
  {
    path: 'list',
    component: DocumentsListComponent,
  },
  {
    path: 'preview/:type/:id',
    component: PreviewDocumentComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppDocumentsRoutingModule {}
