import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPostCardComponent } from './post-card.component';

describe('AppPostCardComponent', () => {
  let component: AppPostCardComponent;
  let fixture: ComponentFixture<AppPostCardComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AppPostCardComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPostCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
