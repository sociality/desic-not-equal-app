import { Component, Input, OnInit } from '@angular/core';
import { PostInterface, POST_TYPES, NEBULAR_STATUS } from '@desic/models';
import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'client-app-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss'],
})
export class AppPostCardComponent implements OnInit {
  @Input() post: PostInterface;
  @Input() type: string;

  postTypes = POST_TYPES;
  backgroundImage: string = '';

  constructor(
    private dialogService: NbDialogService,
    private readonly translate: TranslateService
  ) {}

  isEventPost(post: PostInterface): boolean {
    return post.type === POST_TYPES.EVENT;
  }

  getPostType(post: PostInterface) {
    let postType = '';
    switch (post.type) {
      case POST_TYPES.EVENT:
        postType = 'Events';
        break;
      case POST_TYPES.EDUCATIONAL:
        postType = 'Educationals';
        break;
      case POST_TYPES.POST:
        postType = 'Articles';
        break;
    }
    return postType;
  }

  getCardStatus(post: PostInterface): NEBULAR_STATUS | string {
    let status: NEBULAR_STATUS | string = '';

    switch (post.type) {
      case POST_TYPES.EVENT:
        status = NEBULAR_STATUS.INFO;
        break;
      case POST_TYPES.EDUCATIONAL:
        status = NEBULAR_STATUS.WARNING;
        break;
      case POST_TYPES.POST:
        status = NEBULAR_STATUS.SUCCESS;
        break;
      default:
        status = '';
    }
    return status;
  }

  replaceAccents(value: string) {
    return value
      .replace(/Ά|Α|ά/g, 'α')
      .replace(/Έ|Ε|έ/g, 'ε')
      .replace(/Ή|Η|ή/g, 'η')
      .replace(/Ί|Ϊ|Ι|ί|ΐ|ϊ/g, 'ι')
      .replace(/Ό|Ο|ό/g, 'ο')
      .replace(/Ύ|Ϋ|Υ|ύ|ΰ|ϋ/g, 'υ')
      .replace(/Ώ|Ω|ώ/g, 'ω')
      .replace(/Σ|ς/g, 'σ');
  }

  ngOnInit(): void {
    this.backgroundImage = this.post.image_url
      ? `url(${this.post.image_url})`
      : "url('https://picsum.photos/1200/300?random=1)";
  }
}
