import { AppStateService } from '../../../services/state/state.service';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { APIPostsService } from '@desic/api';
import { PostInterface } from '@desic/models';
import { NbDialogService } from '@nebular/theme';
import { AppPostSingleComponent } from '../post-single/post-single.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'client-app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
})
export class AppPostsListComponent implements OnInit {
  @ViewChild('postWindow', { static: true }) modalTemplate: TemplateRef<any>;
  posts$: Observable<PostInterface[]>;

  postTypes = [
    { label: 'All', value: '' },
    { label: 'Articles', value: 'post' },
    { label: 'Events', value: 'event' },
  ];

  constructor(
    private readonly service: APIPostsService,
    private readonly appStateService: AppStateService,
    private dialogService: NbDialogService,
    private readonly translate: TranslateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Events & News');
    });
    this.posts$ = this.loadPosts();
  }

  private loadPosts() {
    return this.service.getPosts();
  }

  openDialog(post: PostInterface): void {
    this.dialogService.open(AppPostSingleComponent, {
      context: {
        post: post,
      },
      closeOnBackdropClick: true,
    });
  }
}
