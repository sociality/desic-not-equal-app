import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppPostSingleComponent } from './post-single.component';

describe('AppPostSingleComponent', () => {
  let component: AppPostSingleComponent;
  let fixture: ComponentFixture<AppPostSingleComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AppPostSingleComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AppPostSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
