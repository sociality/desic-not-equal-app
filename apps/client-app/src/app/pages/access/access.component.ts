import {
  Component,
  OnInit,
  ViewChild,
  ChangeDetectorRef,
  OnDestroy,
  Input,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { APIAccessService, APIAuthService } from '@desic/api';
import { AppStateService } from '../../services/state/state.service';
import {
  AccessesInterface,
  AccessInterface,
  AuthUserInterface,
  UserInterface,
} from '@desic/models';
import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';

@Component({
  selector: 'client-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.scss'],
})
export class AccessComponent implements OnInit {
  user: AuthUserInterface;

  constructor(
    private translate: TranslateService,
    private authService: NbAuthService,
    private readonly appStateService: AppStateService
  ) {
    this.authService.onTokenChange().subscribe(
      (token: NbAuthJWTToken) => {
        if (token.isValid()) {
          this.user = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable
        }
      },
      (error) => {
        throw error;
      }
    );
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Access Rights');
    });
  }
}
