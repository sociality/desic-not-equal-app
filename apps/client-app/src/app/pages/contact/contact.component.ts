import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { APICommunicationService } from '@desic/api';
import { NbDialogService } from '@nebular/theme';
import { Observable, Subject } from 'rxjs';
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { AppStateService } from '../../services/state/state.service';

@Component({
  selector: 'client-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit, OnDestroy {
  @ViewChild('response_dialog') response_dialog;

  contactForm: FormGroup;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private communicationService: APICommunicationService,
    private readonly appStateService: AppStateService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Contact with us');
    });
    this.initForm();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
  }

  initForm() {
    this.contactForm = this.fb.group({
      email: [, Validators.compose([Validators.required, Validators.email])],
      description: ['', Validators.compose([Validators.required])],
    });
  }

  public async onFormSubmit() {
    if (this.sending) return;

    const controls = this.contactForm.controls;
    /** check form */
    if (this.contactForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }
    this.sending = true;

    const contactData: { email: string; description: string } = {
      email: controls.email.value,
      description: controls.description.value,
    };

    this.sendMessage(contactData);
  }

  sendMessage(contactData: { email: string; description: string }) {
    this.responseHandler(
      this.communicationService.sendMessage(
        contactData.email,
        contactData.description
      )
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/');
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.contactForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
