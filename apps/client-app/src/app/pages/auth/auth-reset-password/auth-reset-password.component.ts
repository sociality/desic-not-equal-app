import { Component } from '@angular/core';
import { NbResetPasswordComponent } from '@nebular/auth';

@Component({
  selector: 'admin-reset-password',
  templateUrl: './auth-reset-password.component.html',
})
export class AuthResetPasswordComponent extends NbResetPasswordComponent {
  logoLegend = 'Admin App';
}
