import { Component } from '@angular/core';
import { NbRequestPasswordComponent } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'admin-request-password',
  templateUrl: './auth-request-password.component.html',
})
export class AuthRequestPasswordComponent extends NbRequestPasswordComponent {
  logoLegend = 'Admin App';
}
