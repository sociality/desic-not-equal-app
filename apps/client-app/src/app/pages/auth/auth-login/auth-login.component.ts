import { ChangeDetectorRef, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NbAuthService, NbLoginComponent } from '@nebular/auth';
import { NbDialogService } from '@nebular/theme';
import { AuthRequestPasswordComponent } from '../auth-request-password/auth-request-password.component';

@Component({
  selector: 'admin-login',
  templateUrl: './auth-login.component.html',
})
export class AuthLoginComponent extends NbLoginComponent {
  logoLegend = 'Admin App';
}
