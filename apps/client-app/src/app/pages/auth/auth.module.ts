import { NbAuthModule } from '@nebular/auth';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { AuthRequestPasswordComponent } from './auth-request-password/auth-request-password.component';
import { AuthResetPasswordComponent } from './auth-reset-password/auth-reset-password.component';

import { ClientAuthRoutingModule } from './auth-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbButtonModule,
  NbTabsetModule,
  NbSelectModule,
  NbRadioModule,
  NbDialogModule,
} from '@nebular/theme';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, RouterOutlet } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [
    AuthLoginComponent,
    AuthRequestPasswordComponent,
    AuthResetPasswordComponent,
  ],
  imports: [
    CommonModule,
    ClientAuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    NbAuthModule,
    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbButtonModule,
    NbTabsetModule,
    NbSelectModule,
    NbRadioModule,
    NbDialogModule.forRoot(),

    RouterModule,
    ViewsModule,
    // BrowserModule
  ],
  entryComponents: [],
})
export class ClientAuthModule {}
