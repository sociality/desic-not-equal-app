export const PERSONAL_MENU_ITEMS = [
  {
    title: 'Access Rights',
    slug: 'access_rights',
    link: 'access',
  },
  {
    title: 'My Documents',
    slug: 'documents',
    link: 'documents',
  },
  {
    title: 'Connections',
    slug: 'connections',
    link: 'connections',
  },
  {
    title: 'Logout',
    link: 'auth/logout',
    slug: 'logout',
    icon: 'log-out-outline',
  },
];

export const MENU_ITEMS = [
  {
    title: 'Home',
    slug: 'home',
    link: '/',
    icon: 'home-outline',
  },
  {
    title: 'Articles & Events',
    slug: 'posts',
    link: '/posts',
    icon: 'file-text-outline',
  },
  {
    title: 'Contact',
    slug: 'contact',
    link: '/contact',
    icon: 'calendar-outline',
  },
];
