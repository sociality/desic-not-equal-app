import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { NbAuthComponent } from '@nebular/auth';

import { AuthGuard } from './auth-guard.service';

import { ClientHomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { AccessComponent } from './pages/access/access.component';
import { ClientLayoutComponent } from './@theme/layouts/layout/layout.component';

const routes: Routes = [
  {
    path: 'auth',
    component: NbAuthComponent,
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.ClientAuthModule),
  },
  {
    path: '',
    component: ClientLayoutComponent,
    children: [
      // {
      //   path: 'auth',
      //   component: NbAuthComponent,
      //   loadChildren: () =>
      //     import('./pages/auth/auth.module').then((m) => m.ClientAuthModule),
      //   //  outlet: 'modal'
      // },
      {
        path: '',
        component: ClientHomeComponent,
        // outlet: null,
      },
      {
        path: 'posts',
        loadChildren: () =>
          import('./pages/posts/posts.module').then((m) => m.AppPostsModule),
      },
      {
        path: 'contact',
        component: ContactComponent,
      },
      {
        path: 'documents',
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('./pages/documents/documents.module').then(
            (m) => m.AppDocumentsModule
          ),
      },
      {
        path: 'connections',
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('./pages/connections/connections.module').then(
            (m) => m.ClientConnectionsModule
          ),
      },
      {
        path: 'access',
        canActivate: [AuthGuard],
        component: AccessComponent,
      },
    ],
  },

  // {
  //   path: '**',
  //   redirectTo: '',
  //   outlet: '',
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class ClientAppRoutingModule {}
