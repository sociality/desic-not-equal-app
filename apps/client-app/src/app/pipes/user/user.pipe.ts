import { identifierModuleUrl } from '@angular/compiler';
import { Pipe, PipeTransform } from '@angular/core';

import { UserCategory, UserRole, InternalChoiceList } from '@desic/models';

@Pipe({
  name: 'user_pipe',
})
export class UserPipe implements PipeTransform {
  constructor() {}

  transform(value: unknown, ...args: unknown[]): unknown {
    if (args[0] == 'access') {
      switch (value) {
        case UserRole.SUPERADMIN:
          return 'Administrator';
          break;
        case UserRole.ADMIN:
          return 'Administrator';
          break;
        case UserRole.USER:
          return 'Member';
          break;
        default:
        // code block
      }
      return;
    } else if (args[0] == 'category') {
      switch (value) {
        case UserCategory.LAW:
          return 'Lawyer';
          break;
        case UserCategory.MEDICAL:
          return 'Psychiatric';
          break;
        case UserCategory.SOCIAL:
          return 'Social Worker';
          break;
        case UserCategory.MANAGEMENT:
          return 'Administrator';
          break;
        case UserCategory.NONE:
          return 'Member';
          break;
        default:
        // code block
      }
      return;
    } else if (args[0] == 'both') {
      if (value['access'] == UserRole.SUPERADMIN) {
        return 'Administrator';
      } else if (value['access'] == UserRole.USER) {
        return 'Member';
      } else {
        switch (value['category']) {
          case UserCategory.LAW:
            return 'Lawyer';
            break;
          case UserCategory.MEDICAL:
            return 'Psychiatric';
            break;
          case UserCategory.SOCIAL:
            return 'Social Worker';
            break;
          case UserCategory.MANAGEMENT:
            return 'Administrator';
            break;
          case UserCategory.NONE:
            return 'Member';
            break;
          default:
          // code block
        }
      }
    }
  }
}
