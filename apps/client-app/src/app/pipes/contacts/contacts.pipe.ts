import { Pipe, PipeTransform } from '@angular/core';

import { InternalContactList, OrganizationSocial } from '@desic/models';
@Pipe({
  name: 'contacts_pipe',
})
export class ContactsPipe implements PipeTransform {
  transform(value: OrganizationSocial[], args: InternalContactList[]): unknown {
    const currentContactsArray = value.map((a) => a.slug);
    const validateContactsList = args.filter(function (el) {
      return currentContactsArray.includes(el.slug);
    });

    return validateContactsList.map((o) => {
      return {
        ...o,
        value: value.filter((ob) => {
          return ob.slug === o.slug;
        })[0].value,
      };
    });
  }
}
