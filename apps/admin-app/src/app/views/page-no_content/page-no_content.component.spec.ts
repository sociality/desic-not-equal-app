import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNoContentComponent } from './page-no_content.component';

describe('PageNoContentComponent', () => {
  let component: PageNoContentComponent;
  let fixture: ComponentFixture<PageNoContentComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PageNoContentComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNoContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
