import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  AbstractControl,
  FormControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { InternalContactList, OrganizationInterface } from '@desic/models';
import { APIOrganizationService } from '@desic/api';
import { AppStateService } from '../../../services/state/state.service';
import { AppChoicesService } from '../../../services/choices/choices.service';

@Component({
  selector: 'admin-edit-organization',
  templateUrl: './edit-organization.component.html',
  styleUrls: ['./edit-organization.component.scss'],
})
export class EditOrganizationComponent implements OnInit, OnDestroy {
  @ViewChild('response_dialog') response_dialog;
  public contactsList: InternalContactList[];

  imageURL: string;
  previousURL: string;
  initialImage: string = '';

  organizationForm: FormGroup;
  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly appStateService: AppStateService,
    private readonly organizationService: APIOrganizationService,
    private readonly choicesService: AppChoicesService
  ) {
    this.contactsList = this.choicesService.getContactsList;
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Members');
    });
    this.initForm();
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  private loadOrganization() {
    return this.organizationService.getOrganization();
  }

  private fetchParameter() {
    return this.loadOrganization()
      .pipe(
        tap((data) => {
          this.initialImage = data.image_url;
          this.organizationForm.patchValue(this.formatDataOnFetch(data));
          console.log(data);
          // this.organizationForm.controls['name'].disable();
          this.patching = true;
          this.changeDetectorRef.detectChanges();
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe();
  }

  initForm() {
    this.organizationForm = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      image_url: ['', Validators.compose([Validators.required])],

      description: ['', Validators.compose([Validators.required])],
      description_2: ['', Validators.compose([Validators.required])],

      email: ['', Validators.compose([Validators.required, Validators.email])],
      phone: ['', Validators.compose([Validators.required])],
      phone_2: ['', Validators.compose([])],
      fax: ['', Validators.compose([])],

      street: ['', Validators.compose([Validators.required])],
      postcode: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],

      social: this.fb.array([], Validators.compose([])),
    });

    this.contactsList.forEach((element) => {
      const social = new FormControl('');
      this.social.push(social);
    });
  }

  get social() {
    return this.organizationForm.get('social') as FormArray;
  }

  private formatDataOnFetch(data: OrganizationInterface) {
    this.previousURL = data.image_url;
    this.imageURL = this.previousURL;
    this.updateValidators(false, ['image_url']);
    return {
      ...data,
      ...data.address,
      social: data.social
        ? this.contactsList
            .map((item) => {
              const obj = data.social.find((o) => o.slug === item.slug);
              return { ...item, ...obj };
            })
            .map((a) => a.value)
        : [],
    };
  }

  private formatDataOnPost(controls: { [key: string]: AbstractControl }) {
    const formData = new FormData();

    formData.append('name', controls.name.value);
    formData.append('image_url', controls.image_url.value);
    formData.append('description', controls.description.value);
    formData.append('description_2', controls.description_2.value);

    formData.append('email', controls.email.value);

    formData.append('phone', controls.phone.value);
    formData.append('phone_2', controls.phone_2.value);
    formData.append('fax', controls.fax.value);

    formData.append('street', controls.street.value);
    formData.append('postcode', controls.postcode.value);
    formData.append('city', controls.city.value);
    formData.append('social', JSON.stringify(this.setContactsValues(controls)));

    return formData;
  }

  setContactsValues(controls: { [key: string]: AbstractControl }) {
    var contacts: InternalContactList[] = [];
    this.contactsList.forEach((value, i) => {
      if (controls.social.value[i]) {
        contacts.push({
          slug: this.contactsList[i].slug,
          value: controls.social.value[i],
        });
      }
    });
    return contacts;
  }

  public onFormSubmit() {
    const controls = this.organizationForm.controls;
    if (this.organizationForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }
    this.sending = true;

    const organizationData: FormData = this.formatDataOnPost(controls);
    this.updateOrganization(organizationData);
  }

  updateOrganization(organizationData: FormData) {
    this.responseHandler(
      this.organizationService.updateOrganization(organizationData)
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      // this.router.navigateByUrl('/clients');
    });
  }

  public onClickBack(userId: string) {
    // this.router.navigateByUrl(`/clients/preview/${userId}`);
  }

  updateValidators(required: boolean, controls: string[]) {
    if (required) {
      controls.forEach((el) => {
        this.organizationForm.controls[el].setValidators([Validators.required]);
        this.organizationForm.controls[el].updateValueAndValidity();
      });
    } else {
      controls.forEach((el) => {
        this.organizationForm.controls[el].clearValidators();
        this.organizationForm.controls[el].updateValueAndValidity();
      });
    }
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.organizationForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
