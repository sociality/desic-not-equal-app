import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
  OnChanges,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  AbstractControl,
  ValidatorFn,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { CategoryInterface, CategoriesInterface } from '@desic/models';
import { APICategoriesService } from '@desic/api';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'admin-edit-categories',
  templateUrl: './edit-categories.component.html',
  styleUrls: ['./edit-categories.component.scss'],
})
export class EditCategoriesComponent implements OnInit, OnDestroy {
  @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;

  type: string;

  categoriesForm: FormGroup;
  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly appStateService: AppStateService,
    private readonly categoriesService: APICategoriesService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Categories');
    });
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  private loadCategories(type: string) {
    return this.categoriesService.getCategories(type);
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        switchMap((params) => {
          this.patching = false;
          const type = params['type'];
          if (type) {
            this.type = type;

            return this.loadCategories(type).pipe(
              tap((data) => {
                this.initForm();
                data.forEach((el) => {
                  this.categories.push(this.createCategoryItem(el));
                });
                this.patching = true;
                this.changeDetectorRef.detectChanges();
              })
            );
          } else {
            this.patching = true;
            return null;
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe();
  }

  private formatDataOnPost(controls: { [key: string]: AbstractControl }) {
    const categories = this.categoriesForm
      .get('categories')
      .value.filter((o) => {
        return o.name && o.slug;
      });
    return {
      categories: categories,
    };
  }

  initForm() {
    this.categoriesForm = this.fb.group({
      categories: this.fb.array(
        [],
        Validators.compose([
          this.atLeastOneInputValidator(),
          this.uniqueValuesValidator(),
        ])
      ),
    });
  }

  get categories() {
    return this.categoriesForm.get('categories') as FormArray;
  }

  createCategoryItem(cat: CategoryInterface): FormGroup {
    return this.fb.group({
      id: [cat.id],
      name: [cat.name],
      slug: [cat.slug],
    });
  }

  insertItem() {
    this.categories.push(this.createCategoryItem({ name: '', slug: '' }));
  }

  removeItem(item: number) {
    this.categories.removeAt(item);
  }

  public onFormSubmit() {
    const controls = this.categoriesForm.controls;
    if (this.categoriesForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.sending = true;

    const categoriesData: CategoriesInterface = this.formatDataOnPost(controls);
    this.openConfirmDialog(categoriesData);
  }

  updateCategories(categoriesData: CategoriesInterface) {
    this.responseHandler(
      this.categoriesService.updateCategories(categoriesData, this.type)
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openConfirmDialog(categoriesData: CategoriesInterface) {
    const dialogRef = this.dialogService.open(this.confirm_dialog);

    dialogRef.onClose.subscribe((result) => {
      if (result) {
        this.updateCategories(categoriesData);
      }
    });
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {});
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.categoriesForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }

  atLeastOneInputValidator(): ValidatorFn {
    return function validate(formGroup: FormGroup) {
      let filled = 0;

      Object.keys(formGroup.controls).forEach((key) => {
        const control = formGroup.controls[key];

        if (control.value) {
          filled++;
        }
      });

      if (filled < 1) {
        return {
          requireInputToBeFilled: true,
        };
      }
      return null;
    };
  }

  uniqueValuesValidator(): ValidatorFn {
    return function validate(formGroup: FormGroup) {
      let uniqueValues = [];
      let hasDuplicate: boolean = false;

      Object.keys(formGroup.controls).forEach((key) => {
        const control = formGroup.controls[key];

        if (uniqueValues.indexOf(control.value.name) != -1) {
          hasDuplicate = true;
        } else {
          uniqueValues.push(control.value.name);
        }
      });

      if (hasDuplicate) {
        return {
          requireValuesToBeUnique: true,
        };
      }
      return null;
    };
  }
}
