import { AdministratorsListComponent } from './administrators-list/administrators-list.component';
import { CreateAdministratorComponent } from './create-administrator/create-administrator.component';
import { PreviewAdministratorComponent } from './preview-administrator/preview-administrator.component';

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'list',
    component: AdministratorsListComponent,
  },
  {
    path: 'create',
    component: CreateAdministratorComponent,
  },
  {
    path: 'edit/:id',
    component: CreateAdministratorComponent,
  },
  {
    path: 'preview/:id',
    component: PreviewAdministratorComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministratorsRoutingModule {}
