import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbRadioModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
  NbListModule,
  NbUserModule,
  NbActionsModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { TranslateModule } from '@ngx-translate/core';

import { AdministratorsRoutingModule } from './administrators-routing.module';

import { AdministratorsListComponent } from './administrators-list/administrators-list.component';
import { CreateAdministratorComponent } from './create-administrator/create-administrator.component';
import { PreviewAdministratorComponent } from './preview-administrator/preview-administrator.component';

import { PipesModule } from '../../pipes/pipes.module';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [
    AdministratorsListComponent,
    CreateAdministratorComponent,
    PreviewAdministratorComponent,
  ],
  imports: [
    CommonModule,

    NbButtonModule,
    NbCardModule,
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbIconModule,
    NbInputModule,
    NbLayoutModule,
    NbRadioModule,
    NbSelectModule,
    NbTabsetModule,
    NbTooltipModule,
    NbEvaIconsModule,
    NbListModule,
    NbUserModule,
    NbActionsModule,

    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

    AdministratorsRoutingModule,

    PipesModule,
    ViewsModule,
  ],
})
export class AdministratorsModule {}
