import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIUsersService } from '@desic/api';
import { UserInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'admin-administrators-list',
  templateUrl: './administrators-list.component.html',
  styleUrls: ['./administrators-list.component.scss'],
})
export class AdministratorsListComponent implements OnInit {
  public users$: Observable<UserInterface[]>;

  constructor(
    private iconLibraries: NbIconLibraries,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private readonly translate: TranslateService,
    private readonly usersService: APIUsersService,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Administrators');
    });
    this.loadUsers();
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('20');
  }

  public onClickPreview(user: UserInterface) {
    this.router.navigate([`/administrators/preview/${user.id}`]);
  }

  public onClickEdit(user: UserInterface) {
    this.router.navigate([`/administrators/edit/${user.id}`]);
  }

  public onClickExportToPDF(user: UserInterface) {
    return;
  }
}
