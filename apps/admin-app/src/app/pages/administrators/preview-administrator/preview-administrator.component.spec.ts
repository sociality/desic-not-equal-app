import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewAdministratorComponent } from './preview-administrator.component';

describe('PreviewAdministratorComponent', () => {
  let component: PreviewAdministratorComponent;
  let fixture: ComponentFixture<PreviewAdministratorComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PreviewAdministratorComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewAdministratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
