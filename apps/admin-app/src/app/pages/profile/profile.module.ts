import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbButtonModule,
  NbTabsetModule,
  NbSelectModule,
  NbRadioModule,
  NbDatepickerModule,
  NbDialogModule,
} from '@nebular/theme';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminProfileRoutingModule } from './profile-routing.module';

@NgModule({
  declarations: [UpdateProfileComponent, UpdatePasswordComponent],
  imports: [
    CommonModule,
    AdminProfileRoutingModule,
    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbButtonModule,
    NbTabsetModule,
    NbSelectModule,
    NbRadioModule,
    NbDialogModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AdminProfileModule {}
