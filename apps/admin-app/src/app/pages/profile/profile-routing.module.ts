import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { UpdatePasswordComponent } from './update-password/update-password.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'profile/personal',
    component: UpdateProfileComponent,
  },
  {
    path: 'profile/password',
    component: UpdatePasswordComponent,
  },
  { path: '**', redirectTo: 'profile/personal' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminProfileRoutingModule {}
