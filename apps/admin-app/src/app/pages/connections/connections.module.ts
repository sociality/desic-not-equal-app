import { ConnectionsListComponent } from './connections-list/connections-list.component';
import { CreateConnectionComponent } from './create-connection/create-connection.component';
import { PeerConnectionComponent } from './peer-connection/peer-connection.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminConnectionsRoutingModule } from './connections-routing.module';
import {
  NbCardModule,
  NbInputModule,
  NbLayoutModule,
  NbButtonModule,
  NbTabsetModule,
  NbSelectModule,
  NbRadioModule,
  NbDialogModule,
  NbChatModule,
  NbIconModule,
  NbListModule,
  NbTooltipModule,
  NbBadgeModule,
  NbUserModule,
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorPickerModule } from 'ngx-color-picker';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { RouterModule } from '@angular/router';
import { LocalConnectionService } from './_connection.service';
import { ViewsModule } from '../../views/views.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ConnectionsListComponent,
    CreateConnectionComponent,
    PeerConnectionComponent,
  ],
  imports: [
    CommonModule,
    AdminConnectionsRoutingModule,

    FormsModule,
    ReactiveFormsModule,

    NbCardModule,
    NbInputModule,
    NbLayoutModule,
    NbButtonModule,
    NbTabsetModule,
    NbSelectModule,
    NbChatModule,
    NbRadioModule,
    NbDialogModule.forRoot(),
    NbListModule,
    NbIconModule,
    NbEvaIconsModule,
    NbTooltipModule,
    NbBadgeModule,
    NbUserModule,
    NbChatModule,
    NbListModule,

    ColorPickerModule,

    RouterModule,

    ViewsModule,
    PipesModule,
  ],
  providers: [],
})
export class AdminConnectionsModule {}
