import { Component, OnInit, ViewChild } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  AbstractControl,
  Validators,
} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { Router } from '@angular/router';
import { APIConnectionsService } from '@desic/api';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'admin-create-connection',
  templateUrl: './create-connection.component.html',
  styleUrls: ['./create-connection.component.scss'],
})
export class CreateConnectionComponent implements OnInit {
  @ViewChild('response_dialog') response_dialog;

  inviteForm: FormGroup;

  constructor(
    private readonly connectionService: APIConnectionsService,
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private router: Router,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.inviteForm = this.fb.group({
      connectionId: [, Validators.compose([Validators.required])],
    });
  }

  public onFormSubmit() {
    const controls = this.inviteForm.controls;
    if (this.inviteForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }

    this.connectionService
      .postConnection(controls.connectionId.value)
      .subscribe(
        (data) => {
          this.openResponseDialog(this.response_dialog, {
            title: 'OK',
            message: data,
          });
        },
        (error) => {
          this.openResponseDialog(this.response_dialog, {
            title: 'Error',
            message: error.message,
          });
        }
      );
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      this.router.navigateByUrl('/members');
    });
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.inviteForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
