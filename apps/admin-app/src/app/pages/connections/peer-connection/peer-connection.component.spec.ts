import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeerConnectionComponent } from './peer-connection.component';

describe('PeerConnectionComponent', () => {
  let component: PeerConnectionComponent;
  let fixture: ComponentFixture<PeerConnectionComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PeerConnectionComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PeerConnectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
