import { ConnectionsListComponent } from './connections-list/connections-list.component';
import { CreateConnectionComponent } from './create-connection/create-connection.component';
import { PeerConnectionComponent } from './peer-connection/peer-connection.component';

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'list',
    component: ConnectionsListComponent,
  },
  {
    path: 'create',
    component: CreateConnectionComponent,
  },
  {
    path: 'peer/:id',
    component: PeerConnectionComponent,
  },
  // {
  //   path: 'edit/:id',
  //   component: AdminCreateStrandComponent,
  // },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminConnectionsRoutingModule {}
