import { NbLoginComponent, NbLogoutComponent } from '@nebular/auth';

import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthLoginComponent } from './auth-login/auth-login.component';
import { AuthRequestPasswordComponent } from './auth-request-password/auth-request-password.component';
import { AuthResetPasswordComponent } from './auth-reset-password/auth-reset-password.component';

export const routes: Routes = [
  {
    path: '',
    component: NbLoginComponent,
  },
  {
    path: 'login',
    component: AuthLoginComponent, // NbLoginComponent,
  },
  // {
  //     path: 'register',
  //     component: NbRegisterComponent,
  // },
  {
    path: 'logout',
    component: NbLogoutComponent,
  },
  {
    path: 'request-password',
    component: AuthRequestPasswordComponent, // NbRequestPasswordComponent,
  },
  {
    path: 'reset-password',
    component: AuthResetPasswordComponent, // NbResetPasswordComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminAuthRoutingModule {}
