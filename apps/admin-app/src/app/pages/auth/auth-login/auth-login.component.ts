import { Component } from '@angular/core';
import { NbLoginComponent } from '@nebular/auth';

@Component({
  selector: 'admin-login',
  templateUrl: './auth-login.component.html',
})
export class AuthLoginComponent extends NbLoginComponent {
  logoLegend = 'Admin App';
}
