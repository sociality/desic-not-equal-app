import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { TranslateModule } from '@ngx-translate/core';

import { IncidentsRoutingModule } from './incidents-routing.module';

import { IncidentsListComponent } from './incidents-list/incidents-list.component';
import { CreateIncidentComponent } from './create-incident/create-incident.component';
import { PreviewIncidentComponent } from './preview-incident/preview-incident.component';

import { PipesModule } from '../../pipes/pipes.module';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [
    IncidentsListComponent,
    CreateIncidentComponent,
    PreviewIncidentComponent,
  ],
  imports: [
    CommonModule,

    NbButtonModule,
    NbCardModule,
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbIconModule,
    NbInputModule,
    NbLayoutModule,
    NbRadioModule,
    NbSelectModule,
    NbTabsetModule,
    NbTooltipModule,
    NbEvaIconsModule,
    NbListModule,
    NbActionsModule,
    NbUserModule,

    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

    IncidentsRoutingModule,

    PipesModule,
    ViewsModule,
  ],
})
export class IncidentsModule {}
