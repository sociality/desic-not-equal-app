import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIIncidentsService } from '@desic/api';
import { IncidentInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'preview-incident',
  templateUrl: './preview-incident.component.html',
  styleUrls: ['./preview-incident.component.scss'],
})
export class PreviewIncidentComponent implements OnInit, OnDestroy {
  @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;
  @ViewChild('delete_dialog') delete_dialog;

  incident$: Observable<IncidentInterface>;

  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly incidentsService: APIIncidentsService,
    private readonly appStateService: AppStateService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Incidents');
    });
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.patching = false;
    this.sending = false;
  }

  private loadIncident(id: string) {
    return this.incidentsService.getIncidentById(id);
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        tap((params) => {
          const id = params['id'];
          if (id) {
            this.incident$ = this.loadIncident(id);
            this.patching = true;
          } else {
            this.patching = true;
            return;
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  public onClickBack() {
    this.router.navigateByUrl(`/incidents/list`);
  }

  public onClickEdit(incidentID: string) {
    this.router.navigateByUrl(`/incidents/edit/${incidentID}`);
  }

  public onClickDelete(incidentID: string, title: string) {
    this.openDeleteDialog(incidentID, title);
  }

  public onClickExport(incidentID: string) {
    this.router.navigateByUrl(`/incidents/edit/${incidentID}`);
  }

  deleteIncident(incidentID: string) {
    this.responseHandler(this.incidentsService.deleteIncident(incidentID));
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = true;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openDeleteDialog(incidentID: string, title: string) {
    const dialogRef = this.dialogService.open(this.delete_dialog, {
      context: { slug: title },
    });
    dialogRef.onClose.subscribe((result) => {
      if (result) {
        this.deleteIncident(incidentID);
      }
    });
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/incidents');
    });
  }
}

// private fetchParameter() {
//   this.activatedRoute.params.subscribe((params) => {
//     const id = params['id'];
//     if (id) {
//       this.loadIncident(id).subscribe((data) => {
//         this.incident = data;
//         this.patching = true;
//         this.changeDetectorRef.markForCheck();
//       });
//     } else {
//       this.patching = true;
//     }
//   }),
//     finalize(() => {
//       this.changeDetectorRef.markForCheck();
//     });
// }
