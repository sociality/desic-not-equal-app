import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewIncidentComponent } from './preview-incident.component';

describe('PreviewIncidentComponent', () => {
  let component: PreviewIncidentComponent;
  let fixture: ComponentFixture<PreviewIncidentComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PreviewIncidentComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewIncidentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
