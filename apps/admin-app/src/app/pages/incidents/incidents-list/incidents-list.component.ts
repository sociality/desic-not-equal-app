import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { NbDialogService, NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIIncidentsService, APIUsersService } from '@desic/api';
import { IncidentInterface, UserInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'incidents-list',
  templateUrl: './incidents-list.component.html',
  styleUrls: ['./incidents-list.component.scss'],
})
export class IncidentsListComponent implements OnInit {
  incidents$: Observable<IncidentInterface[]>;
  users$: Observable<UserInterface[]>;

  public userID: string = '0';
  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private iconLibraries: NbIconLibraries,
    private readonly translate: TranslateService,
    private readonly usersService: APIUsersService,
    private readonly incidentsService: APIIncidentsService,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Incidents');
    });
    this.loadIncidents();
    this.loadUsers();
  }

  private loadIncidents() {
    this.incidents$ = this.incidentsService.getIncidents();
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('10');
  }

  public onClickPreview(incident: IncidentInterface) {
    this.router.navigate([`/incidents/preview/${incident.id}`]);
  }

  public onClickEdit(incident: IncidentInterface) {
    this.router.navigate([`/incidents/edit/${incident.id}`]);
  }

  public onClickExportToPDF(incident: IncidentInterface) {
    return;
  }

  public setSelectedUser(event: Event) {
    this.userID = event.toString();
  }
}
