import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APISessionsService } from '@desic/api';
import { SessionInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'preview-session',
  templateUrl: './preview-session.component.html',
  styleUrls: ['./preview-session.component.scss'],
})
export class PreviewSessionComponent implements OnInit, OnDestroy {
  @ViewChild('confirm_dialog') confirm_dialog;
  @ViewChild('response_dialog') response_dialog;
  @ViewChild('delete_dialog') delete_dialog;

  session$: Observable<SessionInterface>;

  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly sessionsService: APISessionsService,
    private readonly appStateService: AppStateService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Sessions');
    });
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.patching = false;
    this.sending = false;
  }

  private loadSession(id: string) {
    return this.sessionsService.getSessionById(id);
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        tap((params) => {
          const id = params['id'];
          if (id) {
            this.session$ = this.loadSession(id);
            this.patching = true;
          } else {
            this.patching = true;
            return;
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  public onClickBack() {
    this.router.navigateByUrl(`/sessions/list`);
  }

  public onClickEdit(sessionID: string) {
    this.router.navigateByUrl(`/sessions/edit/${sessionID}`);
  }

  public onClickDelete(sessionID: string, title: string) {
    this.openDeleteDialog(sessionID, title);
  }

  public onClickExport(sessionID: string) {
    this.router.navigateByUrl(`/sessions/edit/${sessionID}`);
  }

  deleteSession(sessionID: string) {
    this.responseHandler(this.sessionsService.deleteSession(sessionID));
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = true;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openDeleteDialog(sessionID: string, title: string) {
    const dialogRef = this.dialogService.open(this.delete_dialog, {
      context: { slug: title },
    });
    dialogRef.onClose.subscribe((result) => {
      if (result) {
        this.deleteSession(sessionID);
      }
    });
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/sessions');
    });
  }
}
