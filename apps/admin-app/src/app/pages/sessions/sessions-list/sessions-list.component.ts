import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { NbDialogService, NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APISessionsService, APIUsersService } from '@desic/api';
import { SessionInterface, UserInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'sessions-list',
  templateUrl: './sessions-list.component.html',
  styleUrls: ['./sessions-list.component.scss'],
})
export class SessionsListComponent implements OnInit {
  sessions$: Observable<SessionInterface[]>;
  users$: Observable<UserInterface[]>;

  public userID: string = '0';
  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private iconLibraries: NbIconLibraries,
    private readonly translate: TranslateService,
    private readonly usersService: APIUsersService,
    private readonly sessionsService: APISessionsService,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Sessions');
    });
    this.loadSessions();
    this.loadUsers();
  }

  private loadSessions() {
    this.sessions$ = this.sessionsService.getSessions();
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('10');
  }

  public onClickPreview(session: SessionInterface) {
    this.router.navigate([`/sessions/preview/${session.id}`]);
  }

  public onClickEdit(session: SessionInterface) {
    this.router.navigate([`/sessions/edit/${session.id}`]);
  }

  public onClickExportToPDF(session: SessionInterface) {
    return;
  }

  public setSelectedUser(event: Event) {
    this.userID = event.toString();
  }
}
