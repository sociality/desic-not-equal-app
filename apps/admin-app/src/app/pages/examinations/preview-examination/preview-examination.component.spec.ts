import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewExaminationComponent } from './preview-examination.component';

describe('PreviewExaminationComponent', () => {
  let component: PreviewExaminationComponent;
  let fixture: ComponentFixture<PreviewExaminationComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [PreviewExaminationComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewExaminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
