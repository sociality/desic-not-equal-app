import { ExaminationsListComponent } from './examinations-list/examinations-list.component';
import { CreateExaminationComponent } from './create-examination/create-examination.component';
import { PreviewExaminationComponent } from './preview-examination/preview-examination.component';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: 'list',
    component: ExaminationsListComponent,
  },
  {
    path: 'create',
    component: CreateExaminationComponent,
  },
  {
    path: 'edit/:id',
    component: CreateExaminationComponent,
  },
  {
    path: 'preview/:id',
    component: PreviewExaminationComponent,
  },
  { path: '**', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExaminationsRoutingModule {}
