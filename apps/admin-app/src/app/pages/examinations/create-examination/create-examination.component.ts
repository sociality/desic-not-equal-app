import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormArray,
  Validators,
  AbstractControl,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import {
  APIExaminationsService,
  APICategoriesService,
  APIUsersService,
} from '@desic/api';
import {
  ExaminationInterface,
  CategoryInterface,
  UserInterface,
} from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'create-examination',
  templateUrl: './create-examination.component.html',
  styleUrls: ['./create-examination.component.scss'],
})
export class CreateExaminationComponent implements OnInit, OnDestroy {
  @ViewChild('response_dialog') response_dialog;
  @ViewChild('delete_dialog') delete_dialog;

  examinationID: string = null;

  public categories$: Observable<CategoryInterface[]>;
  public users$: Observable<UserInterface[]>;

  examinationForm: FormGroup;
  patching: boolean = false;
  sending: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly appStateService: AppStateService,
    private readonly usersService: APIUsersService,
    private readonly categoriesService: APICategoriesService,
    private readonly examinationsService: APIExaminationsService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Examinations');
    });
    this.loadCategories();
    this.loadUsers();
    this.initForm();
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.sending = false;
    this.patching = false;
  }

  private loadExamination(id: string) {
    return this.examinationsService.getExaminationById(id);
  }

  private loadCategories() {
    this.categories$ = this.categoriesService.getCategories('examination');
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('10');
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        switchMap(
          (params) => {
            const id = params['id'];
            if (id) {
              this.examinationID = id;

              return this.loadExamination(id).pipe(
                tap((data) => {
                  this.examinationForm.patchValue(this.formatDataOnFetch(data));
                  this.examinationForm.controls['beneficiaryId'].disable();
                  this.patching = true;
                  this.changeDetectorRef.detectChanges();
                })
              );
            } else {
              this.patching = true;
              return Promise.resolve(true);
            }
          },
          (error) => {
            console.log(`Fetch Data Error || ${error}`);
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.detectChanges();
        })
      )
      .subscribe();
  }

  private formatDataOnFetch(data: ExaminationInterface) {
    return data;
  }

  private formatDataOnPost(controls: { [key: string]: AbstractControl }) {
    return {
      title: controls.title.value,
      description: controls.description.value,
      examinationDate: controls.examinationDate.value,
      category: controls.category.value,
      keywords: controls.keywords.value,
      beneficiaryId: controls.beneficiaryId.value
        ? controls.beneficiaryId.value
        : '0',
    };
  }

  initForm() {
    this.examinationForm = this.fb.group({
      beneficiaryId: [, Validators.compose([Validators.required])],

      title: ['', Validators.compose([Validators.required])],
      description: ['', Validators.compose([Validators.required])],

      examinationDate: ['', Validators.compose([Validators.required])],
      category: ['', Validators.compose([Validators.required])],
      keywords: ['', Validators.compose([Validators.required])],
    });
  }

  public onFormSubmit() {
    if (this.sending) return;

    const controls = this.examinationForm.controls;
    if (this.examinationForm.invalid) {
      Object.keys(controls).forEach((controlName) =>
        controls[controlName].markAsTouched()
      );
      return;
    }
    this.sending = true;

    const examinationData: ExaminationInterface = this.formatDataOnPost(
      controls
    );
    this.examinationID
      ? this.updateExamination(this.examinationID, examinationData)
      : this.createExamination(examinationData);
  }

  createExamination(examinationData: ExaminationInterface) {
    this.responseHandler(
      this.examinationsService.postExamination(examinationData)
    );
  }

  updateExamination(
    examinationID: string,
    examinationData: ExaminationInterface
  ) {
    this.responseHandler(
      this.examinationsService.updateExamination(examinationID, examinationData)
    );
  }

  responseHandler(message$: Observable<string>) {
    message$
      .pipe(
        tap(
          (data) => {
            this.openResponseDialog(this.response_dialog, {
              status: true,
              title: 'OK',
              message: data,
            });
          },
          (error) => {
            this.openResponseDialog(this.response_dialog, {
              status: false,
              title: 'Error',
              message: error.message,
            });
          }
        ),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.sending = false;
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      if (_data.status) this.router.navigateByUrl('/examinations');
    });
  }

  public onClickBack(examinationId: string) {
    this.router.navigateByUrl(`/examinations/preview/${examinationId}`);
  }

  isControlHasError(controlName: string, validationType: string): boolean {
    const control = this.examinationForm.controls[controlName];
    if (!control) {
      return false;
    }

    const result =
      control.hasError(validationType) && (control.dirty || control.touched);
    return result;
  }
}
