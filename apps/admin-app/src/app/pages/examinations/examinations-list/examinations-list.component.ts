import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { NbDialogService, NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIExaminationsService, APIUsersService } from '@desic/api';
import { ExaminationInterface, UserInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'examinations-list',
  templateUrl: './examinations-list.component.html',
  styleUrls: ['./examinations-list.component.scss'],
})
export class ExaminationsListComponent implements OnInit {
  examinations$: Observable<ExaminationInterface[]>;
  users$: Observable<UserInterface[]>;

  public userID: string = '0';
  constructor(
    private router: Router,
    private dialogService: NbDialogService,
    private iconLibraries: NbIconLibraries,
    private readonly translate: TranslateService,
    private readonly usersService: APIUsersService,
    private readonly examinationsService: APIExaminationsService,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Examinations');
    });
    this.loadExaminations();
    this.loadUsers();
  }

  private loadExaminations() {
    this.examinations$ = this.examinationsService.getExaminations();
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('10');
  }

  public onClickPreview(examination: ExaminationInterface) {
    this.router.navigate([`/examinations/preview/${examination.id}`]);
  }

  public onClickEdit(examination: ExaminationInterface) {
    this.router.navigate([`/examinations/edit/${examination.id}`]);
  }

  public onClickExportToPDF(examination: ExaminationInterface) {
    return;
  }

  public setSelectedUser(event: Event) {
    this.userID = event.toString();
  }
}
