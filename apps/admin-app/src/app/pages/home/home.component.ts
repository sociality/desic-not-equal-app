import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AppStateService } from '../../services/state/state.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';

import { APIStatisticsService } from '@desic/api';
import { finalize, takeUntil, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'admin-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class AdminHomeComponent implements OnInit {
  public barChartOptions: ChartOptions = {
    responsive: true,
  };
  public barChartLabels: Label[] = [
    '2006',
    '2007',
    '2008',
    '2009',
    '2010',
    '2011',
    '2012',
  ];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [];

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B' },
  ];

  patching: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private readonly appStateService: AppStateService,
    private readonly statisticsService: APIStatisticsService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Home');
    });
    this.fetchData();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.patching = false;
  }

  loadStatistics() {
    return this.statisticsService.getStatistics();
  }

  private fetchData() {
    this.loadStatistics()
      .pipe(
        tap((data) => {
          this.barChartLabels = data.map((a) => {
            return a.month;
          });
          this.barChartData = [
            {
              data: data.map((a) => {
                return a.sessions;
              }),
              label: 'Sessions',
            },
            {
              data: data.map((a) => {
                return a.incidents;
              }),
              label: 'Incidents',
            },
            {
              data: data.map((a) => {
                return a.examinations;
              }),
              label: 'Examinations',
            },
          ];
          this.patching = true;
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }
}
