import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { finalize, switchMap, takeUntil, tap } from 'rxjs/operators';

import { NbDialogService } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIUsersService } from '@desic/api';
import { UserInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'admin-preview-client',
  templateUrl: './preview-client.component.html',
  styleUrls: ['./preview-client.component.scss'],
})
export class PreviewClientComponent implements OnInit {
  @ViewChild('response_dialog') response_dialog;

  user$: Observable<UserInterface>;

  patching: boolean = false;
  private unsubscribe: Subject<any>;

  constructor(
    private dialogService: NbDialogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private translate: TranslateService,
    private readonly usersService: APIUsersService,
    private readonly appStateService: AppStateService
  ) {
    this.unsubscribe = new Subject();
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Members');
    });
    this.fetchParameter();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    this.patching = false;
  }

  private loadUser(id: string) {
    return this.usersService.getUserById(id);
  }

  private fetchParameter() {
    this.activatedRoute.params
      .pipe(
        tap((params) => {
          const id = params['id'];
          if (id) {
            this.user$ = this.loadUser(id);
            this.patching = true;
          } else {
            this.patching = true;
            return;
          }
        }),
        takeUntil(this.unsubscribe),
        finalize(() => {
          this.changeDetectorRef.markForCheck();
        })
      )
      .subscribe();
  }

  public onClickBack() {
    this.router.navigateByUrl(`/clients/list`);
  }

  public onClickEdit(id: string) {
    this.router.navigateByUrl(`/clients/edit/${id}`);
  }

  public onClickExportToPDF(id: string) {
    return;
  }

  protected openResponseDialog(_dialog, _data) {
    const dialogRef = this.dialogService.open(_dialog, { context: _data });

    dialogRef.onClose.subscribe((result) => {
      this.router.navigateByUrl('/members');
    });
  }
}
