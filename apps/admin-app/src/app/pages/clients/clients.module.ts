import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbDatepickerModule,
  NbDialogModule,
  NbIconModule,
  NbInputModule,
  NbLayoutModule,
  NbListModule,
  NbRadioModule,
  NbSelectModule,
  NbTabsetModule,
  NbTooltipModule,
  NbUserModule,
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { TranslateModule } from '@ngx-translate/core';

import { ClientsRoutingModule } from './clients-routing.module';

import { ClientsListComponent } from './clients-list/clients-list.component';
import { CreateClientComponent } from './create-client/create-client.component';
import { PreviewClientComponent } from './preview-client/preview-client.component';

import { PipesModule } from '../../pipes/pipes.module';
import { ViewsModule } from '../../views/views.module';

@NgModule({
  declarations: [
    ClientsListComponent,
    CreateClientComponent,
    PreviewClientComponent,
  ],
  imports: [
    CommonModule,

    NbButtonModule,
    NbCardModule,
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbIconModule,
    NbInputModule,
    NbLayoutModule,
    NbRadioModule,
    NbSelectModule,
    NbTabsetModule,
    NbTooltipModule,
    NbEvaIconsModule,
    NbUserModule,
    NbActionsModule,
    NbListModule,

    FormsModule,
    ReactiveFormsModule,
    TranslateModule,

    ClientsRoutingModule,

    PipesModule,
    ViewsModule,
  ],
})
export class ClientsModule {}
