import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { NbDialogService, NbIconLibraries } from '@nebular/theme';
import { TranslateService } from '@ngx-translate/core';

import { APIUsersService } from '@desic/api';
import { UserInterface } from '@desic/models';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'admin-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.scss'],
})
export class ClientsListComponent implements OnInit {
  public users$: Observable<UserInterface[]>;

  constructor(
    private iconLibraries: NbIconLibraries,
    private router: Router,
    private changeDetectorRef: ChangeDetectorRef,
    private readonly translate: TranslateService,
    private readonly usersService: APIUsersService,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Members');
    });
    this.loadUsers();
  }

  private loadUsers() {
    this.users$ = this.usersService.getUsersByAccess('10');
  }

  public onClickPreview(user: UserInterface) {
    this.router.navigate([`/clients/preview/${user.id}`]);
  }

  public onClickEdit(user: UserInterface) {
    this.router.navigate([`/clients/edit/${user.id}`]);
  }

  public onClickExportToPDF(user: UserInterface) {
    return;
  }
}
