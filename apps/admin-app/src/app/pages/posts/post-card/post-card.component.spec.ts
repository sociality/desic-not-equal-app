import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPostCardComponent } from './post-card.component';

describe('AdminPostCardComponent', () => {
  let component: AdminPostCardComponent;
  let fixture: ComponentFixture<AdminPostCardComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AdminPostCardComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPostCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
