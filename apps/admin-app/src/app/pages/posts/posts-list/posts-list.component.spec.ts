import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPostsListComponent } from './posts-list.component';

describe('AdminPostsListComponent', () => {
  let component: AdminPostsListComponent;
  let fixture: ComponentFixture<AdminPostsListComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AdminPostsListComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPostsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
