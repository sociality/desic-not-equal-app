import { Observable } from 'rxjs';
import { APIPostsService } from '@desic/api';
import { PostInterface } from '@desic/models';

import { Component, OnInit } from '@angular/core';
import { AppStateService } from '../../../services/state/state.service';

@Component({
  selector: 'admin-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.scss'],
})
export class AdminPostsListComponent implements OnInit {
  posts$: Observable<PostInterface[]>;
  constructor(
    private readonly service: APIPostsService,
    private readonly appStateService: AppStateService
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.appStateService.setPageTitle('Posts');
    });
    this.loadPosts();
  }

  private loadPosts() {
    this.posts$ = this.service.getPosts();
  }
}
