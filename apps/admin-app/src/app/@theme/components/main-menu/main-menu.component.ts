import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { MENU_ITEMS } from '../../../menu-items.config';
import { NbMenuItem, NbIconLibraries } from '@nebular/theme';
import { NbAuthJWTToken, NbAuthService } from '@nebular/auth';
import { Observable, Subscription } from 'rxjs';
import { AppStateService } from '../../../services/state/state.service';
import { APIAuthService } from '@desic/api';
import { LocalConnectionService } from '../../../pages/connections/_connection.service';
import { LocalConnectionInterface } from '../../../pages/connections/_connection.interface';

interface MenuItem extends NbMenuItem {
  slug: string;
}

@Component({
  selector: 'admin-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
})
export class AdminMainMenuComponent {
  items: MenuItem[] = MENU_ITEMS;
  user = {};

  public sessions: LocalConnectionInterface['PeerSessions'] = [];
  private subscription: Subscription = new Subscription();

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private iconLibraries: NbIconLibraries,
    private authService: NbAuthService,
    private localConnectionService: LocalConnectionService,
    private readonly appStateService: AppStateService,
    private autenticationService: APIAuthService
  ) {
    this.subscription.add(
      this.localConnectionService.peerSessions.subscribe(
        (data) => (this.sessions = data),
        (error) => {
          throw error;
        }
      )
    );

    this.iconLibraries.registerFontPack('font-awesome', {
      iconClassPrefix: 'fa',
    });

    this.localConnectionService.peerSessions.subscribe(
      (data) => {
        setTimeout(() => {
          this.items[this.items.findIndex((obj) => obj.slug === 'connections')][
            'badge'
          ] = {
            text: this.getTotalUnread().toString(),
            status: 'warning',
          };
        });
        this.changeDetectorRef.markForCheck();
      },
      (error) => {
        throw error;
      }
    );

    this.authService.onTokenChange().subscribe(
      (token: NbAuthJWTToken) => {
        if (token.isValid()) {
          this.user = token.getPayload(); // here we receive a payload from the token and assigns it to our `user` variable

          this.getAccessRights();
        }
      },
      (error) => {
        throw error;
      }
    );
  }

  getTotalUnread() {
    return this.sessions.reduce((accum, item) => accum + item.unread, 0);
  }

  getAccessRights() {
    this.items[this.items.findIndex((obj) => obj.slug === 'administrators')][
      'hidden'
    ] = this.autenticationService.isClient || this.autenticationService.isAdmin;

    this.items[this.items.findIndex((obj) => obj.slug === 'members')][
      'hidden'
    ] =
      this.autenticationService.isClient ||
      (this.autenticationService.isAdmin &&
        !this.autenticationService.isManagement);

    this.items[this.items.findIndex((obj) => obj.slug === 'settings')][
      'hidden'
    ] =
      this.autenticationService.isClient ||
      (this.autenticationService.isAdmin &&
        !this.autenticationService.isManagement);

    this.items[this.items.findIndex((obj) => obj.slug === 'incidents')][
      'hidden'
    ] =
      this.autenticationService.isClient ||
      (this.autenticationService.isAdmin &&
        !this.autenticationService.isSocial);

    this.items[this.items.findIndex((obj) => obj.slug === 'sessions')][
      'hidden'
    ] =
      this.autenticationService.isClient ||
      (this.autenticationService.isAdmin && !this.autenticationService.isLaw);

    this.items[this.items.findIndex((obj) => obj.slug === 'examinations')][
      'hidden'
    ] =
      this.autenticationService.isClient ||
      (this.autenticationService.isAdmin &&
        !this.autenticationService.isMedical);

    this.items[this.items.findIndex((obj) => obj.slug === 'posts')]['hidden'] =
      this.autenticationService.isClient ||
      (this.autenticationService.isAdmin &&
        !this.autenticationService.isManagement);
  }
}
