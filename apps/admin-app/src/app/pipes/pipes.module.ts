import { NgModule } from '@angular/core';

import { OrderByPipe } from './order_by.pipe';
import { AccessPipe } from './access/access.pipe';
import { UserPipe } from './user/user.pipe';
import { FilterPipe } from './filter/filter.pipe';

@NgModule({
  imports: [],
  declarations: [OrderByPipe, AccessPipe, UserPipe, FilterPipe],
  exports: [OrderByPipe, AccessPipe, UserPipe, FilterPipe],
})
export class PipesModule {}
