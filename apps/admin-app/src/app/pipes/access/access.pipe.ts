import { Pipe, PipeTransform } from '@angular/core';

import { AccessCategory } from '@desic/models';
@Pipe({
  name: 'access_pipe',
})
export class AccessPipe implements PipeTransform {
  transform(value: unknown, ...args: unknown[]): unknown {
    if (args[0] == 'category') {
      switch (value) {
        case AccessCategory.ALL:
          return 'All Documents';
          break;
        case AccessCategory.SOCIAL:
          return 'Social Documents';
          break;
        case AccessCategory.MEDICAL:
          return 'Meidcal Documents';
          break;
        case AccessCategory.LAW:
          return 'Law Documents';
          break;
        default:
        // code block
      }
      return;
    } else if (args[0] == 'action') {
      return 'Read & Write';
    }
  }
}
