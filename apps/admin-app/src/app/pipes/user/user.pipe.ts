import { identifierModuleUrl } from '@angular/compiler';
import { Pipe, PipeTransform } from '@angular/core';

import { UserCategory, UserRole, InternalChoiceList } from '@desic/models';
import { AppChoicesService } from '../../services/choices/choices.service';
@Pipe({
  name: 'user_pipe',
})
export class UserPipe implements PipeTransform {
  constructor(private readonly choicesService: AppChoicesService) {}

  transform(value: unknown, ...args: unknown[]): unknown {
    if (args[0] == 'access') {
      switch (value) {
        case UserRole.SUPERADMIN:
          return 'Administrator';
          break;
        case UserRole.ADMIN:
          return 'Administrator';
          break;
        case UserRole.USER:
          return 'Member';
          break;
        default:
        // code block
      }
      return;
    } else if (args[0] == 'category') {
      switch (value) {
        case UserCategory.LAW:
          return 'Lawyer';
          break;
        case UserCategory.MEDICAL:
          return 'Psychiatric';
          break;
        case UserCategory.SOCIAL:
          return 'Social Worker';
          break;
        case UserCategory.MANAGEMENT:
          return 'Administrator';
          break;
        case UserCategory.NONE:
          return 'Member';
          break;
        default:
        // code block
      }
      return;
    } else if (args[0] == 'maritalStatus') {
      const choices: InternalChoiceList[] = this.choicesService
        .getMaritalStatusList;
      return choices.find((o: InternalChoiceList) => {
        return o.value == value;
      }).title;
    } else if (args[0] == 'employmentStatus') {
      const choices: InternalChoiceList[] = this.choicesService
        .getEmploymentStatusList;
      return choices.find((o: InternalChoiceList) => {
        return o.value == value;
      }).title;
    }
  }
}
