import { Pipe, PipeTransform } from '@angular/core';

import {
  IncidentInterface,
  SessionInterface,
  ExaminationInterface,
} from '@desic/models';
@Pipe({
  name: 'filter_pipe',
})
export class FilterPipe implements PipeTransform {
  transform(value: any[], ...args: unknown[]): any[] {
    if (args[0] != '0') {
      return value.filter((o) => {
        return o.beneficiaryId == args[0];
      });
    } else {
      return value;
    }
  }
}
