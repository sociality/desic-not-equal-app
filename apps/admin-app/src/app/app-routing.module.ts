import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NbAuthComponent } from '@nebular/auth';

import { AuthGuard } from './auth-guard.service';

import { AdminLayoutComponent } from './@theme/layouts/layout/layout.component';
import { AdminHomeComponent } from './pages/home/home.component';

export const routes: Routes = [
  {
    path: 'auth',
    component: NbAuthComponent,
    loadChildren: () =>
      import('./pages/auth/auth.module').then((m) => m.AdminAuthModule),
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: AdminLayoutComponent,
    children: [
      {
        path: '',
        component: AdminHomeComponent,
      },
      {
        path: 'clients',
        loadChildren: () =>
          import('./pages/clients/clients.module').then((m) => m.ClientsModule),
      },
      {
        path: 'administrators',
        loadChildren: () =>
          import('./pages/administrators/administrators.module').then(
            (m) => m.AdministratorsModule
          ),
      },
      {
        path: 'incidents',
        loadChildren: () =>
          import('./pages/incidents/incidents.module').then(
            (m) => m.IncidentsModule
          ),
      },
      {
        path: 'sessions',
        loadChildren: () =>
          import('./pages/sessions/sessions.module').then(
            (m) => m.SessionsModule
          ),
      },
      {
        path: 'posts',
        loadChildren: () =>
          import('./pages/posts/posts.module').then((m) => m.AdminPostsModule),
      },
      {
        path: 'connections',
        loadChildren: () =>
          import('./pages/connections/connections.module').then(
            (m) => m.AdminConnectionsModule
          ),
      },
      {
        path: 'examinations',
        canActivate: [AuthGuard],
        loadChildren: () =>
          import('./pages/examinations/examinations.module').then(
            (m) => m.ExaminationsModule
          ),
      },
      {
        path: 'settings',
        loadChildren: () =>
          import('./pages/settings/settings.module').then(
            (m) => m.AdminSettingsModule
          ),
      },
      {
        path: 'profile',
        loadChildren: () =>
          import('./pages/profile/profile.module').then(
            (m) => m.AdminProfileModule
          ),
      },
    ],
  },
  // {
  //   path: 'progress',
  //   component: PageProgressComponent,
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AdminAppRoutingModule {}
