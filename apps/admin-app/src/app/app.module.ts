/** Desic */
import { ApiModule, AppConfig } from '@desic/api';
import { UiModule } from '@desic/ui';

/** Modules */
import { AppComponent } from './app.component';
import { AdminAppRoutingModule } from './app-routing.module';

import { ThemeModule } from './@theme/theme.module';

/** Angular Modules */
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/** Components */
import { AdminHomeComponent } from './pages/home/home.component';

/** Modules */
import { PipesModule } from './pipes/pipes.module';
import { ViewsModule } from './views/views.module';

/** Nebular Modules */
import {
  NbThemeModule,
  NbLayoutModule,
  NbSidebarModule,
  NbMenuModule,
  NbIconModule,
} from '@nebular/theme';
import {
  NbPasswordAuthStrategy,
  NbAuthModule,
  NbAuthJWTToken,
  NbAuthJWTInterceptor,
  NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
} from '@nebular/auth';

import { NbEvaIconsModule } from '@nebular/eva-icons';
import {
  HttpClient,
  HttpClientModule,
  HTTP_INTERCEPTORS,
} from '@angular/common/http';

import { AuthGuard } from './auth-guard.service';

/** ngx-translate Modules */
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import {
  MissingTranslationHandler,
  MissingTranslationHandlerParams,
} from '@ngx-translate/core';
import { ChartsModule } from 'ng2-charts';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18/', '.json');
}

export class MyMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    return '<<this text is untranslated>>';
  }
}

const ANGULAR_MODULES = [
  BrowserModule,
  BrowserAnimationsModule,
  HttpClientModule,
];

const NEBULAR_MODULES = [
  NbThemeModule.forRoot({ name: 'default' }),
  NbLayoutModule,
  NbEvaIconsModule,
  NbSidebarModule.forRoot(),
  NbMenuModule.forRoot(),
  NbIconModule,

  NbAuthModule.forRoot({
    strategies: [
      NbPasswordAuthStrategy.setup({
        name: 'email',
        token: {
          class: NbAuthJWTToken,
          key: 'token',
        },
        baseEndpoint: AppConfig.API_URL,
        login: {
          endpoint: '/authentication/sign_in',
          method: 'post',
          redirect: {
            success: '/',
            failure: null, // stay on the same page
          },
        },
        logout: {
          endpoint: '/authentication/sign_out',
          method: 'delete',
          redirect: {
            success: '/auth/login',
            failure: null, // stay on the same page
          },
        },
        requestPass: {
          endpoint: '/authentication/request_pass',
          method: 'post',
          redirect: {
            success: '/auth/login',
            failure: null, // stay on the same page
          },
        },
        resetPass: {
          endpoint: '/authentication/restore_pass',
          method: 'post',
          resetPasswordTokenKey: 'tokenKey',
          redirect: {
            success: '/auth/login',
            failure: null, // stay on the same page
          },
        },
      }),
    ],
    forms: {},
  }),
];

const ADMIN_MODULES = [AdminAppRoutingModule, ThemeModule];

const SHARED_MODULES = [UiModule, ApiModule];

const TRANSLATE_MODULE = [
  TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: HttpLoaderFactory,
      deps: [HttpClient],
    },
    missingTranslationHandler: {
      provide: MissingTranslationHandler,
      useClass: MyMissingTranslationHandler,
    },
    useDefaultLang: true,
  }),
];

@NgModule({
  declarations: [AppComponent, AdminHomeComponent],
  imports: [
    ...ANGULAR_MODULES,
    ...NEBULAR_MODULES,
    ...SHARED_MODULES,
    ...ADMIN_MODULES,
    PipesModule,
    ViewsModule,

    ChartsModule,

    ...TRANSLATE_MODULE,
  ],
  exports: [TranslateModule],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: NbAuthJWTInterceptor, multi: true },
    {
      provide: NB_AUTH_TOKEN_INTERCEPTOR_FILTER,
      useValue: function () {
        return false;
      },
    },
    AuthGuard,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
