import { ColorService } from './services/color.service';
import { NbLayoutModule } from '@nebular/theme';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule, NbLayoutModule],
  providers: [ColorService],
})
export class UiModule {}
