interface OrganizationAddress {
  street: string;
  postcode: string;
  city: string;
}

export interface OrganizationSocial {
  slug: string;
  value: string;
}

export interface OrganizationInterface {
  id: number;
  name: string;
  image_url: string;

  email: string;
  phone: string;
  phone_2: string;
  fax: string;

  address: OrganizationAddress;
  social: OrganizationSocial[];

  description: string;
  description_2: string;
}
