import { LanguageContentInterface } from './language-content.interface';
// import { CourseInterface } from './course.interface';

export interface PostInterface {
  id?: number;
  slug: string;
  title: LanguageContentInterface[];
  description: LanguageContentInterface[];
  when?: Date;
  where?: string;
  type: POST_TYPES;
  image_url?: string;
  // course?: CourseInterface;
  // courseID?: number;
  createdAt?: Date;
  contentFiles?: string[];
}

export enum POST_TYPES {
  EVENT = 'event',
  EDUCATIONAL = 'educational',
  POST = 'post',
}
