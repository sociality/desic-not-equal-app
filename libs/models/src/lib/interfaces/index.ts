export * from './api-message.interface';
export * from './api-response.interface';
export * from './user.interface';
export * from './access.interface';

export * from './organization.interface';

export * from './language-content.interface';
export * from './language.interface';
export * from './strand.interface';
export * from './sub-strand.interface';
export * from './weekly-schedule.interface';
export * from './post.interface';
export * from './nebular.interface';

export * from './categories.interface';
export * from './incident.interface';
export * from './session.interface';
export * from './examination.interface';
export * from './connection.interface';
export * from './statistics.interface';

export * from './_internal.interface';
