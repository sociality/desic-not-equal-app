import { UserInterface } from './user.interface';

export interface ConnectionInterface {
  id: string;

  secret: string;

  sender: UserInterface;
  senderId: string;
  receiver: UserInterface;
  receiverId: string;
}
