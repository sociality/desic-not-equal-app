export interface StatisticsInterface {
  month: string;
  examinations: number;
  sessions: number;
  incidents: number;
}
