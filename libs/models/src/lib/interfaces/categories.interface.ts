export enum CATEGORY_TYPES {
  SESSION = 'session',
  INCIDENT = 'incident',
}

export interface CategoryInterface {
  id?: number;
  slug: string;
  name: string;
  type?: CATEGORY_TYPES;
}

export interface CategoriesInterface {
  categories: CategoryInterface[];
}
