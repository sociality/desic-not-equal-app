export interface APIMessage {
  message: string;
  success: boolean;
}
