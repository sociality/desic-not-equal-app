export interface LanguageContentInterface {
  lang: string;
  content: string;
}
