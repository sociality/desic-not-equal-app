export interface APIResponse<T> {
  data: APIReponseData<T>;
  success: boolean;
}

// TODO: How to define an interface with property name ?
export interface APIReponseData<T> {
  [key: string]: T;
}
