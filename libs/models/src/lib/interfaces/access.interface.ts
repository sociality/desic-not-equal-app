import { UserInterface } from './user.interface';

export enum AccessEditor {
  SUPERADMIN = 'superadmin',
  USER = 'user',
}

export enum AccessCategory {
  LAW = 40,
  MEDICAL = 30,
  SOCIAL = 20,
  ALL = 10,
}

export enum AccessAction {
  COMMUNICATE = 30,
  READ = 20,
  WRITE = 10,
}

export enum AccessStatus {
  REVOKE = 'revoke',
  NEUTRAL = 'neutral',
  INVOKE = 'invoke',
}

export interface AccessInterface extends UserInterface {
  accessId: number;
  accessCategory: string;
  accessAction: string;
  accessEditor: string;
  accessStatus: string;
}

export interface AccessesInterface {
  accesses: AccessInterface[];
}
