import { StrandInterface } from './strand.interface';
import { LanguageContentInterface } from './language-content.interface';

export interface SubstrandInterface {
  id?: number;
  slug: string;
  title: LanguageContentInterface[];
  description: LanguageContentInterface[];
  strand?: StrandInterface;
  strandID?: number;
  contentFiles?: string[];
}
