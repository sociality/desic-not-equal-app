import { LanguageContentInterface } from './language-content.interface';
import { SubstrandInterface } from './sub-strand.interface';

export interface StrandInterface {
  id?: number;
  slug: string;
  title: LanguageContentInterface[];
  description: LanguageContentInterface[];
  color: string;
  substrands?: SubstrandInterface[];
  image_url?: string;
  strandID?: number;
}
