export interface LanguageInterface {
  id?: number;
  slug: string;
  name: string;
  dir: string;
}
