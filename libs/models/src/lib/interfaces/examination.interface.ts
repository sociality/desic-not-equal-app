import { UserInterface } from './user.interface';

export interface ExaminationInterface {
  id?: string;

  title: string;
  description: string;

  examinationDate: Date;

  category: string;
  keywords: string;

  benefactor?: UserInterface;
  benefactorId?: string;
  beneficiary?: UserInterface;
  beneficiaryId: string;
}
