export interface WeeklyScheduleInterface {
  day: string;
  starting_time: string;
  ending_time: string;
}
