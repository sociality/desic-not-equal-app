import { IncidentInterface, APIResponse, APIMessage } from '@desic/models';
import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APIIncidentsService {
  constructor(private http: HttpClient) {}

  getIncidents(): Observable<IncidentInterface[]> {
    return this.http
      .get<APIResponse<IncidentInterface[]>>(URL.forIncidents())
      .pipe(map((response) => response.data.incidents));
  }

  getIncidentById(id: string): Observable<IncidentInterface> {
    return this.http
      .get<APIResponse<IncidentInterface>>(`${URL.forIncidents()}/${id}`)
      .pipe(map((response) => response.data.incident));
  }

  getIncidentsByClient(): Observable<IncidentInterface[]> {
    return this.http
      .get<APIResponse<IncidentInterface[]>>(`${URL.forIncidents()}/client`)
      .pipe(map((response) => response.data.incidents));
  }

  postIncident(incident: IncidentInterface): Observable<string> {
    return this.http
      .post<APIMessage>(URL.forIncidents(), incident)
      .pipe(map((response: APIMessage) => response.message));
  }

  updateIncident(id: string, incident: IncidentInterface): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forIncidents()}/${id}`, incident)
      .pipe(map((response: APIMessage) => response.message));
  }

  deleteIncident(id: string): Observable<string> {
    return this.http
      .delete<APIMessage>(`${URL.forIncidents()}/${id}`)
      .pipe(map((response: APIMessage) => response.message));
  }
}
