import { TestBed } from '@angular/core/testing';

import { APIIncidentsService } from './incidents.service';

describe('IncidentsService', () => {
  let service: APIIncidentsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIIncidentsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
