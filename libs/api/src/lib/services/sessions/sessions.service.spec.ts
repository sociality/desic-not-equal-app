import { TestBed } from '@angular/core/testing';

import { APISessionsService } from './sessions.service';

describe('SessionsService', () => {
  let service: APISessionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APISessionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
