import { SessionInterface, APIResponse, APIMessage } from '@desic/models';
import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APISessionsService {
  constructor(private http: HttpClient) {}

  getSessions(): Observable<SessionInterface[]> {
    return this.http
      .get<APIResponse<SessionInterface[]>>(URL.forSessions())
      .pipe(map((response) => response.data.sessions));
  }

  getSessionById(id: string): Observable<SessionInterface> {
    return this.http
      .get<APIResponse<SessionInterface>>(`${URL.forSessions()}/${id}`)
      .pipe(map((response) => response.data.session));
  }

  getSessionsByClient(): Observable<SessionInterface[]> {
    return this.http
      .get<APIResponse<SessionInterface[]>>(`${URL.forSessions()}/client`)
      .pipe(map((response) => response.data.sessions));
  }

  postSession(session: SessionInterface): Observable<string> {
    return this.http
      .post<APIMessage>(URL.forSessions(), session)
      .pipe(map((response: APIMessage) => response.message));
  }

  updateSession(id: string, session: SessionInterface): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forSessions()}/${id}`, session)
      .pipe(map((response: APIMessage) => response.message));
  }

  deleteSession(id: string): Observable<string> {
    return this.http
      .delete<APIMessage>(`${URL.forSessions()}/${id}`)
      .pipe(map((response: APIMessage) => response.message));
  }
}
