import {
  APIResponse,
  CategoryInterface,
  APIMessage,
  CategoriesInterface,
} from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APICommunicationService {
  constructor(private http: HttpClient) {}

  sendMessage(sender: string, message: string): Observable<string> {
    return this.http
      .post<APIMessage>(`${URL.forCommunication()}`, {
        sender: sender,
        message: message,
      })
      .pipe(map((response) => response.message));
  }
}
