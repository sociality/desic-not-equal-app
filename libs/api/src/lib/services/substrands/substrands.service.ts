import { APIResponse, APIMessage, SubstrandInterface } from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APISubstrandsService {
  constructor(private http: HttpClient) {}

  getSubstrands(): Observable<SubstrandInterface[]> {
    return this.http
      .get<APIResponse<SubstrandInterface[]>>(URL.forSubstrands())
      .pipe(map((response) => response.data.substrands));
  }

  postSubstrand(substrand: SubstrandInterface): Observable<string> {
    return this.http
      .post<APIMessage>(URL.forSubstrands(), substrand)
      .pipe(map((response: APIMessage) => response.message));
  }

  updateSubstrand(
    id: number,
    substrand: SubstrandInterface
  ): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forSubstrands()}/${id}`, substrand)
      .pipe(map((response: APIMessage) => response.message));
  }

  deleteSubstrand(id: number): Observable<string> {
    return this.http
      .delete<APIMessage>(`${URL.forSubstrands()}/${id}`)
      .pipe(map((response: APIMessage) => response.message));
  }

  postContentImage(image: FormData): Observable<any> {
    return this.http
      .post<any>(`${URL.forSubstrands()}/image`, image)
      .pipe(map((response: any) => response));
  }
}
