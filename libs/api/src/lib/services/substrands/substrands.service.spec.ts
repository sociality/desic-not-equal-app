import { TestBed } from '@angular/core/testing';

import { APISubstrandsService } from './substrands.service';

describe('SubstrandsService', () => {
  let service: APISubstrandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APISubstrandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
