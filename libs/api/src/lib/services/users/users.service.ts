import { APIResponse, APIMessage, UserInterface } from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APIUsersService {
  constructor(private http: HttpClient) {}

  getUsersByAccess(access: string): Observable<UserInterface[]> {
    return this.http
      .get<APIResponse<UserInterface[]>>(`${URL.forUsers()}/access/${access}`)
      .pipe(map((response) => response.data.users));
  }

  getUserById(id: string): Observable<UserInterface> {
    return this.http
      .get<APIResponse<UserInterface>>(`${URL.forUsers()}/${id}`)
      .pipe(map((response) => response.data.user));
  }

  updateUser(id: string, user: UserInterface): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forUsers()}/${id}`, user)
      .pipe(map((response: APIMessage) => response.message));
  }

  deleteUser(id: number): Observable<string> {
    return this.http
      .delete<APIMessage>(`${URL.forUsers()}/${id}`)
      .pipe(map((response: APIMessage) => response.message));
  }
}
