import { TestBed } from '@angular/core/testing';

import { APIStrandsService } from './strands.service';

describe('APIStrandsService', () => {
  let service: APIStrandsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIStrandsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
