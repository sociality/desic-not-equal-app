import { STRANDS_MOCK } from './../../mocks/strands.mock';
import { APIResponse, APIMessage, StrandInterface } from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APIStrandsService {
  constructor(private http: HttpClient) {}

  getStrands(): Observable<StrandInterface[]> {
    return this.http
      .get<APIResponse<StrandInterface[]>>(URL.forStrands())
      .pipe(map((response) => response.data.strands));
    // return of(STRANDS_MOCK);
  }

  postStrand(strand: FormData): Observable<string> {
    return this.http
      .post<APIMessage>(URL.forStrands(), strand)
      .pipe(map((response: APIMessage) => response.message));
  }

  updateStrand(id: number, strand: FormData): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forStrands()}/${id}`, strand)
      .pipe(map((response: APIMessage) => response.message));
  }

  deleteStrand(id: number): Observable<string> {
    return this.http
      .delete<APIMessage>(`${URL.forStrands()}/${id}`)
      .pipe(map((response: APIMessage) => response.message));
  }
}
