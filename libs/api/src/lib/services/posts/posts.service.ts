import { APIResponse, APIMessage, PostInterface } from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APIPostsService {
  constructor(private http: HttpClient) {}

  getPosts(): Observable<PostInterface[]> {
    return this.http
      .get<APIResponse<PostInterface[]>>(URL.forPosts())
      .pipe(map((response) => response.data.posts));
  }

  getPostById(id: number): Observable<PostInterface> {
    return this.http
      .get<APIResponse<PostInterface>>(`${URL.forPosts()}/${id}`)
      .pipe(map((response) => response.data.post));
  }

  postPost(post: FormData): Observable<string> {
    return this.http
      .post<APIMessage>(URL.forPosts(), post)
      .pipe(map((response: APIMessage) => response.message));
  }

  updatePost(id: number, post: FormData): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forPosts()}/${id}`, post)
      .pipe(map((response: APIMessage) => response.message));
  }

  deletePost(id: number): Observable<string> {
    return this.http
      .delete<APIMessage>(`${URL.forPosts()}/${id}`)
      .pipe(map((response: APIMessage) => response.message));
  }

  postContentImage(image: FormData): Observable<any> {
    return this.http
      .post<any>(`${URL.forPosts()}/image`, image)
      .pipe(map((response: any) => response));
  }
}
