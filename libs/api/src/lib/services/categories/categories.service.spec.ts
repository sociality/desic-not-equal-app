import { TestBed } from '@angular/core/testing';

import { APICategoriesService } from './categories.service';

describe('CategoriesService', () => {
  let service: APICategoriesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APICategoriesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
