import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import {
  UserInterface,
  AuthUserInterface,
  APIResponse,
  APIMessage,
  UserRole,
  UserCategory,
} from '@desic/models';
import { URL } from './../../url.constants';

@Injectable({
  providedIn: 'root',
})
export class APIAuthService {
  constructor(private http: HttpClient) {}

  createUser(user: any): Observable<string> {
    console.log(user);
    return this.http
      .post<APIMessage>(`${URL.forAuth()}/register_user`, { ...user })
      .pipe(map((response: APIMessage) => response.message));
  }

  public get currentUser(): AuthUserInterface {
    const auth_app_token = JSON.parse(localStorage.getItem('auth_app_token'));

    let authUser: AuthUserInterface = null;
    if (auth_app_token) {
      authUser = JSON.parse(atob(auth_app_token.value.split('.')[1]));
    }

    return authUser;
  }

  // private accessRights(access: string, min: number, max: number): boolean {
  //   return ((parseInt(access) > min) && (parseInt(access) < max));
  // }
  //

  public get isSuperAdmin(): boolean {
    if (this.currentUser)
      return this.currentUser.access === UserRole.SUPERADMIN;
    return null;
  }

  public get isAdmin(): boolean {
    if (this.currentUser) return this.currentUser.access === UserRole.ADMIN;
    //  return this.accessRights(this.currentUser.access, 15, 25);
    return null;
  }

  public get isClient(): boolean {
    if (this.currentUser) return this.currentUser.access === UserRole.USER;
    //  return this.accessRights(this.currentUser.access, 5, 15);
    return null;
  }

  public get isLaw(): boolean {
    if (this.currentUser)
      return (
        this.currentUser.access === UserRole.ADMIN &&
        this.currentUser.category === UserCategory.LAW
      );
    //return this.accessRights(this.currentUser.access, 15, 25) && this.accessRights(this.currentUser.category, 35, 45);
    return null;
  }

  public get isMedical(): boolean {
    if (this.currentUser)
      return (
        this.currentUser.access === UserRole.ADMIN &&
        this.currentUser.category === UserCategory.MEDICAL
      );
    //   return this.accessRights(this.currentUser.access, 15, 25) && this.accessRights(this.currentUser.category, 25, 35);
    return null;
  }

  public get isSocial(): boolean {
    if (this.currentUser)
      return (
        this.currentUser.access === UserRole.ADMIN &&
        this.currentUser.category === UserCategory.SOCIAL
      );
    //  return this.accessRights(this.currentUser.access, 15, 25) && this.accessRights(this.currentUser.category, 15, 25);
    return null;
  }

  public get isManagement(): boolean {
    if (this.currentUser)
      return (
        this.currentUser.access === UserRole.ADMIN &&
        this.currentUser.category === UserCategory.MANAGEMENT
      );
    //    return this.accessRights(this.currentUser.access, 15, 25) && this.accessRights(this.currentUser.category, 5, 15);
    return null;
  }
}
