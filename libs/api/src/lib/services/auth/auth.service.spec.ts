import { TestBed } from '@angular/core/testing';

import { APIAuthService } from './auth.service';

describe('AuthService', () => {
  let service: APIAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
