import { TestBed } from '@angular/core/testing';

import { APIExaminationsService } from './examinations.service';

describe('ExaminationsService', () => {
  let service: APIExaminationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIExaminationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
