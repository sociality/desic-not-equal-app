import { ExaminationInterface, APIResponse, APIMessage } from '@desic/models';
import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APIExaminationsService {
  constructor(private http: HttpClient) {}

  getExaminations(): Observable<ExaminationInterface[]> {
    return this.http
      .get<APIResponse<ExaminationInterface[]>>(URL.forExaminations())
      .pipe(map((response) => response.data.examinations));
  }

  getExaminationById(id: string): Observable<ExaminationInterface> {
    return this.http
      .get<APIResponse<ExaminationInterface>>(`${URL.forExaminations()}/${id}`)
      .pipe(map((response) => response.data.examination));
  }

  getExaminationsByClient(): Observable<ExaminationInterface[]> {
    return this.http
      .get<APIResponse<ExaminationInterface[]>>(
        `${URL.forExaminations()}/client`
      )
      .pipe(map((response) => response.data.examinations));
  }

  postExamination(examination: ExaminationInterface): Observable<string> {
    return this.http
      .post<APIMessage>(URL.forExaminations(), examination)
      .pipe(map((response: APIMessage) => response.message));
  }

  updateExamination(
    id: string,
    examination: ExaminationInterface
  ): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forExaminations()}/${id}`, examination)
      .pipe(map((response: APIMessage) => response.message));
  }

  deleteExamination(id: string): Observable<string> {
    return this.http
      .delete<APIMessage>(`${URL.forExaminations()}/${id}`)
      .pipe(map((response: APIMessage) => response.message));
  }
}
