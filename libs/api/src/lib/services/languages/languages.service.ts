import { APIResponse, LanguageInterface, APIMessage } from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APILanguagesService {
  constructor(private http: HttpClient) {}

  getLanguages(): Observable<LanguageInterface[]> {
    return this.http
      .get<APIResponse<LanguageInterface[]>>(URL.forLanguages())
      .pipe(map((response) => response.data.languages));
  }

  updateLanguages(languages: {
    languages: LanguageInterface[];
  }): Observable<string> {
    return this.http
      .put<APIMessage>(URL.forLanguages(), languages)
      .pipe(map((response: APIMessage) => response.message));
  }
}
