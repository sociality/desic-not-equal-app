import { APIResponse, APIMessage, OrganizationInterface } from '@desic/models';

import { URL } from './../../url.constants';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class APIOrganizationService {
  constructor(private http: HttpClient) {}

  getOrganization(): Observable<OrganizationInterface> {
    return this.http
      .get<APIResponse<OrganizationInterface>>(`${URL.forOrganization()}`)
      .pipe(map((response) => response.data.organization));
  }

  updateOrganization(organization: FormData): Observable<string> {
    return this.http
      .put<APIMessage>(`${URL.forOrganization()}/`, organization)
      .pipe(map((response: APIMessage) => response.message));
  }
}
