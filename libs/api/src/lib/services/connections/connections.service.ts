import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

import { APIResponse, APIMessage, ConnectionInterface } from '@desic/models';

import { URL } from './../../url.constants';

@Injectable({
  providedIn: 'root',
})
export class APIConnectionsService {
  constructor(private http: HttpClient) {}

  getConnectionsAdmin(): Observable<ConnectionInterface[]> {
    return this.http
      .get<APIResponse<ConnectionInterface[]>>(`${URL.forConnections()}/admin`)
      .pipe(map((response) => response.data.connections));
    // return of(STRANDS_MOCK);
  }

  getConnectionsClient(): Observable<ConnectionInterface[]> {
    return this.http
      .get<APIResponse<ConnectionInterface[]>>(`${URL.forConnections()}/client`)
      .pipe(map((response) => response.data.connections));
    // return of(STRANDS_MOCK);
  }

  getConnectionsById(id: string): Observable<ConnectionInterface> {
    return this.http
      .get<APIResponse<ConnectionInterface>>(`${URL.forConnections()}/${id}`)
      .pipe(map((response) => response.data.connection));
  }

  postConnection(connectionId: string): Observable<ConnectionInterface> {
    return this.http
      .post<APIResponse<ConnectionInterface>>(URL.forConnections(), {
        connectionId: connectionId,
      })
      .pipe(map((response) => response.data.connection));
  }
}
