import { TestBed } from '@angular/core/testing';

import { APIStatisticsService } from './statistics.service';

describe('APIStatisticsService', () => {
  let service: APIStatisticsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(APIStatisticsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
