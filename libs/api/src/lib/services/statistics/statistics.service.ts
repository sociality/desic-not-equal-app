import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { APIResponse, StatisticsInterface } from '@desic/models';
import { URL } from './../../url.constants';

@Injectable({
  providedIn: 'root',
})
export class APIStatisticsService {
  constructor(private http: HttpClient) {}

  getStatistics(): Observable<StatisticsInterface[]> {
    return this.http
      .get<APIResponse<StatisticsInterface[]>>(`${URL.forStatistics()}/`)
      .pipe(map((response) => response.data.statistics));
  }
}
