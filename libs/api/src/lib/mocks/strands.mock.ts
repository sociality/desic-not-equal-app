export const STRANDS_MOCK = [
  {
    id: 1,
    slug: 'literacy',
    title: [
      {
        lang: 'en',
        content: 'Literacy',
      },
      {
        lang: 'el',
        content: 'Ανάγνωση',
      },
    ],
    description: [
      {
        lang: 'en',
        content: 'A description for literacy in english',
      },
      {
        lang: 'el',
        content: 'Μια περιγραφή για literacy στα ελληνικά',
      },
    ],
    color: 'red',
    substrands: [
      {
        id: 1,
        slug: 'homework_support',
        title: [
          {
            lang: 'en',
            content: 'Homework support',
          },
          {
            lang: 'el',
            content: 'Υποστήριξη στο Σπίτι',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'A description for homework_support in english',
          },
          {
            lang: 'el',
            content: 'Μια περιγραφή για homework_support στα ελληνικά',
          },
        ],
      },
      {
        id: 2,
        slug: 'mother_tongue_substrands',
        title: [
          {
            lang: 'en',
            content: 'Mother tongue substrands',
          },
          {
            lang: 'el',
            content: 'Δραστηριότητες στη μητρική Γλώσσα',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'A description for mother_tongue_substrands in english',
          },
          {
            lang: 'el',
            content: 'Μια περιγραφή για mother_tongue_substrands στα ελληνικά',
          },
        ],
      },
      {
        id: 6,
        slug: 'greek',
        title: [
          {
            lang: 'en',
            content: 'Greek (4levels)',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Greek (4levels)',
          },
        ],
      },
      {
        id: 7,
        slug: 'english',
        title: [
          {
            lang: 'en',
            content: 'English & other languages',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'English & other\nlanguages',
          },
        ],
      },
    ],
  },
  {
    id: 2,
    slug: 'art_creativity',
    title: [
      {
        lang: 'en',
        content: 'Art & Creativity',
      },
    ],
    description: [
      {
        lang: 'en',
        content: 'Art & Creativity',
      },
    ],
    color: 'green',
    substrands: [
      {
        id: 3,
        slug: 'poetry',
        title: [
          {
            lang: 'en',
            content: 'Poetry',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'A description for poetry in english',
          },
        ],
      },
      {
        id: 4,
        slug: 'photography',
        title: [
          {
            lang: 'en',
            content: 'Photography',
          },
          {
            lang: 'es',
            content: 'Spanish Phtography',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'A description for photography in english',
          },
          {
            lang: 'es',
            content: 'Description in Spanish',
          },
        ],
      },
      {
        id: 8,
        slug: 'visual',
        title: [
          {
            lang: 'en',
            content: 'Visual arts',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Visual arts',
          },
        ],
      },
      {
        id: 9,
        slug: 'film',
        title: [
          {
            lang: 'en',
            content: 'Film & digital storytelling',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Film & digital\nstorytelling',
          },
        ],
      },
      {
        id: 10,
        slug: 'crafts',
        title: [
          {
            lang: 'en',
            content: 'Crafts',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Crafts',
          },
        ],
      },
      {
        id: 11,
        slug: 'music',
        title: [
          {
            lang: 'en',
            content: 'Music',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Music',
          },
        ],
      },
      {
        id: 12,
        slug: 'dance',
        title: [
          {
            lang: 'en',
            content: 'Dance',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Dance',
          },
        ],
      },
    ],
  },
  {
    id: 5,
    slug: 'information-ref',
    title: [
      {
        lang: 'en',
        content: 'Information & Referral Pathway',
      },
    ],
    description: [
      {
        lang: 'en',
        content: 'Information\n& Referral\nPathway',
      },
    ],
    color: '#944a4a',
    substrands: [
      {
        id: 13,
        slug: 'legal',
        title: [
          {
            lang: 'en',
            content: 'Legal rights',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Legal rights',
          },
        ],
      },
      {
        id: 14,
        slug: 'reproductive',
        title: [
          {
            lang: 'en',
            content: 'Reproductive health',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Reproductive health',
          },
        ],
      },
      {
        id: 15,
        slug: 'gbv',
        title: [
          {
            lang: 'en',
            content: 'GBV Training',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'GBV Training',
          },
        ],
      },
      {
        id: 16,
        slug: 'mental',
        title: [
          {
            lang: 'en',
            content: 'Mental health',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Mental health',
          },
        ],
      },
      {
        id: 17,
        slug: 'social-rights',
        title: [
          {
            lang: 'en',
            content: 'Social rights',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Social rights',
          },
        ],
      },
      {
        id: 18,
        slug: 'labour-rights',
        title: [
          {
            lang: 'en',
            content: 'Labour rights',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Labour rights',
          },
        ],
      },
    ],
  },
  {
    id: 4,
    slug: 'phsyco-social',
    title: [
      {
        lang: 'en',
        content: 'Psycho- Social Support',
      },
    ],
    description: [
      {
        lang: 'en',
        content: 'Psycho-\nSocial\nSupport',
      },
    ],
    color: '#ff0000',
    substrands: [
      {
        id: 19,
        slug: 'individual-counseling',
        title: [
          {
            lang: 'en',
            content: 'Individual counseling',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Individual counseling',
          },
        ],
      },
      {
        id: 20,
        slug: 'drama-therapy',
        title: [
          {
            lang: 'en',
            content: 'Drama therapy',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Drama therapy',
          },
        ],
      },
      {
        id: 21,
        slug: 'music-movement',
        title: [
          {
            lang: 'en',
            content: 'Music & movement therapy',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Music & movement therapy',
          },
        ],
      },
      {
        id: 22,
        slug: 'EMDR',
        title: [
          {
            lang: 'en',
            content: 'EMDR',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'EMDR',
          },
        ],
      },
    ],
  },
  {
    id: 7,
    slug: 'media',
    title: [
      {
        lang: 'en',
        content: 'Media & Advocacy',
      },
    ],
    description: [
      {
        lang: 'en',
        content: 'Media &\nAdvocacy',
      },
    ],
    color: '#5b2424',
    substrands: [
      {
        id: 23,
        slug: 'media-use',
        title: [
          {
            lang: 'en',
            content: 'Media use',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Media use',
          },
        ],
      },
      {
        id: 24,
        slug: 'interview-training',
        title: [
          {
            lang: 'en',
            content: 'Interview training',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Interview training',
          },
        ],
      },
      {
        id: 25,
        slug: 'public-speaking',
        title: [
          {
            lang: 'en',
            content: 'Public speaking',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Public speaking',
          },
        ],
      },
      {
        id: 26,
        slug: 'social-media',
        title: [
          {
            lang: 'en',
            content: 'Social media',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Social media',
          },
        ],
      },
      {
        id: 27,
        slug: 'creative-writing',
        title: [
          {
            lang: 'en',
            content: 'Creative writing',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Creative writing',
          },
        ],
      },
      {
        id: 28,
        slug: 'personal-narratives',
        title: [
          {
            lang: 'en',
            content: 'Personal narratives',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Personal narratives',
          },
        ],
      },
    ],
  },
  {
    id: 6,
    slug: 'skills',
    title: [
      {
        lang: 'en',
        content: 'Skills & Capacity Building',
      },
    ],
    description: [
      {
        lang: 'en',
        content: 'Skills &\nCapacity\nBuilding',
      },
    ],
    color: '#2a1414',
    substrands: [
      {
        id: 29,
        slug: 'first-aid',
        title: [
          {
            lang: 'en',
            content: 'First Aid',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'First Aid',
          },
        ],
      },
      {
        id: 30,
        slug: 'leadership-training',
        title: [
          {
            lang: 'en',
            content: 'Leadership training',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Leadership training',
          },
        ],
      },
      {
        id: 31,
        slug: 'IT-Coding',
        title: [
          {
            lang: 'en',
            content: 'IT & Coding',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'IT & Coding',
          },
        ],
      },
      {
        id: 32,
        slug: 'CV-writing',
        title: [
          {
            lang: 'en',
            content: 'CV writing',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'CV writing',
          },
        ],
      },
      {
        id: 33,
        slug: 'cooking-sewing',
        title: [
          {
            lang: 'en',
            content: 'Cooking & sewing',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Cooking & sewing',
          },
        ],
      },
      {
        id: 34,
        slug: 'infant-massage',
        title: [
          {
            lang: 'en',
            content: 'Infant massage & care',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Infant massage & care',
          },
        ],
      },
      {
        id: 35,
        slug: 'digital-design',
        title: [
          {
            lang: 'en',
            content: 'Digital design',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Digital design',
          },
        ],
      },
    ],
  },
  {
    id: 8,
    slug: 'community',
    title: [
      {
        lang: 'en',
        content: 'Self-care & Community Engagement',
      },
    ],
    description: [
      {
        lang: 'en',
        content: 'Self-care &\nCommunity\nEngagement',
      },
    ],
    color: '#928383',
    substrands: [
      {
        id: 36,
        slug: 'stress-management',
        title: [
          {
            lang: 'en',
            content: 'Stress management',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Stress management',
          },
        ],
      },
      {
        id: 37,
        slug: 'meditation',
        title: [
          {
            lang: 'en',
            content: 'Meditation',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Meditation',
          },
        ],
      },
      {
        id: 38,
        slug: 'yoga',
        title: [
          {
            lang: 'en',
            content: 'Yoga',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Yoga',
          },
        ],
      },
      {
        id: 39,
        slug: 'dance-2',
        title: [
          {
            lang: 'en',
            content: 'Dance',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Dance',
          },
        ],
      },
      {
        id: 40,
        slug: 'gyro-kinetics',
        title: [
          {
            lang: 'en',
            content: 'Gyro kinetics & Feldenkreis method',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Gyro kinetics &\nFeldenkreis method',
          },
        ],
      },
      {
        id: 41,
        slug: 'self-defense',
        title: [
          {
            lang: 'en',
            content: 'Self-defense',
          },
        ],
        description: [
          {
            lang: 'en',
            content: 'Self-defense',
          },
        ],
      },
    ],
  },
];
