export const EVENTS_MOCK = [
  {
    id: 1,
    slug: 'homework_support',
    title: [
      { lang: 'en', content: 'Homework support' },
      { lang: 'el', content: 'Υποστήριξη στο Σπίτι' },
    ],
    description: [
      { lang: 'en', content: 'A description for homework_support in english' },
      {
        lang: 'el',
        content: 'Μια περιγραφή για homework_support στα ελληνικά',
      },
    ],
    strand: {
      id: 1,
      slug: 'literacy',
      title: [
        { lang: 'en', content: 'Literacy' },
        { lang: 'el', content: 'Ανάγνωση' },
      ],
      description: [
        { lang: 'en', content: 'A description for literacy in english' },
        { lang: 'el', content: 'Μια περιγραφή για literacy στα ελληνικά' },
      ],
      color: 'red',
    },
  },
  {
    id: 2,
    slug: 'mother_tongue_substrands',
    title: [
      { lang: 'en', content: 'Mother tongue substrands' },
      { lang: 'el', content: 'Δραστηριότητες στη μητρική Γλώσσα' },
    ],
    description: [
      {
        lang: 'en',
        content: 'A description for mother_tongue_substrands in english',
      },
      {
        lang: 'el',
        content: 'Μια περιγραφή για mother_tongue_substrands στα ελληνικά',
      },
    ],
    strand: {
      id: 1,
      slug: 'literacy',
      title: [
        { lang: 'en', content: 'Literacy' },
        { lang: 'el', content: 'Ανάγνωση' },
      ],
      description: [
        { lang: 'en', content: 'A description for literacy in english' },
        { lang: 'el', content: 'Μια περιγραφή για literacy στα ελληνικά' },
      ],
      color: 'blue',
    },
  },
  {
    id: 3,
    slug: 'poetry',
    title: [
      { lang: 'en', content: 'Poetry' },
      { lang: 'el', content: 'Ποίηση' },
    ],
    description: [
      { lang: 'en', content: 'A description for poetry in english' },
      { lang: 'el', content: 'Μια περιγραφή για poetry στα ελληνικά' },
    ],
    strand: {
      id: 2,
      slug: 'art_&_creativity',
      title: [
        { lang: 'en', content: 'Art & Creativity' },
        { lang: 'el', content: 'Τέχνη & Δημιουργηκότητα' },
      ],
      description: [
        {
          lang: 'en',
          content: 'A description for art_&_creativity in english',
        },
        {
          lang: 'el',
          content: 'Μια περιγραφή για art_&_creativity στα ελληνικά',
        },
      ],
      color: 'green',
    },
  },
  {
    id: 4,
    slug: 'photography',
    title: [
      { lang: 'en', content: 'Photography' },
      { lang: 'el', content: 'Φωτογραφία' },
    ],
    description: [
      { lang: 'en', content: 'A description for photography in english' },
      { lang: 'el', content: 'Μια περιγραφή για photography στα ελληνικά' },
    ],
    strand: {
      id: 2,
      slug: 'art_&_creativity',
      title: [
        { lang: 'en', content: 'Art & Creativity' },
        { lang: 'el', content: 'Τέχνη & Δημιουργηκότητα' },
      ],
      description: [
        {
          lang: 'en',
          content: 'A description for art_&_creativity in english',
        },
        {
          lang: 'el',
          content: 'Μια περιγραφή για art_&_creativity στα ελληνικά',
        },
      ],
      color: 'green',
    },
  },
];
